# Hello, Flask!
from flask import Flask, render_template, request
import pandas as pd
from bokeh.plotting import Figure
from bokeh.resources import CDN
from bokeh.embed import components, file_html
from bokeh.util.string import encode_utf8
from bokeh.resources import INLINE
#from models import db


def report():
    app = Flask(__name__)
    return app

app = report()

# Index page

@app.route('/')
def index():
    #return render_template("child.html", result = 'click the button to get the result')
    return render_template("child.html")

@app.route('/child')
def index2():
    return render_template("child.html", result = 'click the button to get the result')

@app.route('/feb2018featureaudit') #to delete
def feb2018featureaudit():
    return render_template("/reports/featureaudit/feb2018featureaudit.html")

@app.route('/featureaudit',  methods=['GET', 'POST'])
def featureaudit():
    from pythonscripts.mixpanel.featureaudit.feature_audit_tabulate2 import PlotDefault, WhereParameter
    import datetime
    selected_segment='All'
    selected_events = 'All'
    month1 = month2 = ''
    if request.method == 'POST':
        selected_segment = request.form['segment']
        month1 = request.form['month1']
        month2 = request.form['month2']
        selected_events = request.form['choose_events']
        where = WhereParameter(segment = selected_segment) if selected_segment != "" else WhereParameter()
    else:
        where = WhereParameter()
    
    choose_all_events_bool = False if selected_events == 'essential_events' else True
    from_date = '2017-08-01'
    to_date = str(datetime.date.today())
    test = PlotDefault(from_date=from_date, to_date = to_date, where=where, choose_all_events_bool =choose_all_events_bool)
    
    feature_audit_chart = test.chart_html_file
    table_num_users_for_each_event_as_html = test.table_num_users_for_each_event_as_html

    if month1 == "" or month2 == "":
        month1 = month2 = '<blank>'
    else:
        selected_segment = 'All' if selected_segment=='' else selected_segment
        feature_audit_chart = test.plot_difference(month1,month2,selected_segment)


    return render_template("/reports/featureaudit/featureauditmain.html", \
        feature_audit_chart = feature_audit_chart, selected_segment=selected_segment,\
        table_num_users_for_each_event = table_num_users_for_each_event_as_html, month1=month1, month2=month2, selected_events=selected_events  )

@app.route('/stickinessratio', methods=['GET', 'POST'])
def mauwaudau():
    '''
    import pythonscripts.mixpanel.mauwaudau_tabulate as m
    master = m.return_master()
    mauwaudau_table = master['join_tables_html']
    mauwaudau_table = {'a':1,'b':2}
    '''
    from pythonscripts.mixpanel.stickiness_ratio.stickiness_ratio_main import ConstructRatioTables
    obj = ConstructRatioTables(from_date='2017-06-01', to_date='2018-03-31')
    table = obj.overall_table
    insert_mauwaudau_table = obj.get_html_to_df(table)
    return render_template("/reports/mauboardmetrics/mauwaudau.html", insert_mauwaudau_table = insert_mauwaudau_table)

@app.route('/lastseen_All')
def lastseen_All():
    #from pythonscripts.mixpanel.lastseenengagement.lastseen import value
    from pythonscripts.mixpanel.lastseenengagement.lastseen_postman import GetLastSeenDataFromMixpanel
    pull_data = GetLastSeenDataFromMixpanel("./static/data/last_seen_engagement_data.csv")
    data = pull_data.get_html()
    #data=1
    #open(os.path.join(os.path.dirname(__file__)
    return render_template("/reports/lastseenengagement/lastseen_All.html", data=data)   

@app.route('/contactexportutilization',  methods=['GET', 'POST'])
def contactexportutilization():
    #import pythonscripts.visage.connectpsql2 as psql2
    from pythonscripts.visage.connectpsql2 import ContactExportUtilization
    selected_segment=''
    if request.method == 'POST':
        selected_segment = request.form['segment']
        main = ContactExportUtilization(segment=selected_segment)

        return render_template("/reports/featureutilization/contactexportutilization_segment.html", \
            type = 'Contact', type2='Export', selected_segment = selected_segment, \
            insert_table =  main.contact_percentile_table, insert_table2 = main.export_percentile_table,\
            htmlfilename_contact = main.contact_buckets_plot, htmlfilename_export = main.export_buckets_plot) 
    else:
        main = ContactExportUtilization(segment=selected_segment)
        return render_template("/reports/featureutilization/contactexportutilization.html", \
            contact_table = main.contact_percentile_table , export_table= main.export_percentile_table,\
            linechart_contact=main.contact_buckets_plot,linechart_export=main.export_buckets_plot)

@app.route('/cohortanalysis',  methods=["GET","POST"])
def cohortanalysis(name = None):
    if request.method == 'POST':
        to_refresh = request.form['refresh']
        if to_refresh == 'true':
           import pythonscripts.cohortanalysis.Code.cleanpaidusersdata #to rerun scripts to get updated charts. only need to refresh if u upload new data
           import datetime
           return render_template("/reports/cohortanalysis/cohortanalysis.html", updatedat = "Last Updated:" + datetime.datetime.today().isoformat()) 
           
    return render_template("/reports/cohortanalysis/cohortanalysis.html")

@app.route('/howtouse')
def howtouse():
    return render_template("/reports/howtouse/howtouse.html")

@app.route('/exec' , methods=['GET', 'POST'])
def parse(name=None):
    if request.method == "POST":
        return 'exec this is a post method'
    else:
        import pythonscripts.Parse as p
        #return "Hello World"
        res = str(p.add(5,5))
        return render_template('child.html',name=name, result = res)



# With debug=True, Flask server will auto-reload 
# when there are code changes
if __name__ == '__main__':
	app.run(debug=True)
    # app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
    #SQLALCHEMY_DATABASE_URI = 'postgresql://<psql_username>:<psql_password>@localhost/flask_family_recipes_app'
    #SQLALCHEMY_TRACK_MODIFICATIONS = True
