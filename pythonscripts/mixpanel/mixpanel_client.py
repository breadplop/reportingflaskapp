#! /usr/bin/env python
#
# Mixpanel, Inc. -- http://mixpanel.com/
#
# Python API client library to consume mixpanel.com analytics data.
#
# Copyright 2010-2013 Mixpanel, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import base64
import urllib.request

try:
    import json
except ImportError:
    import simplejson as json

class Mixpanel(object):

    #ENDPOINT = 'https://data.mixpanel.com/api'
    ENDPOINT = 'https://mixpanel.com/api'
    VERSION = '2.0'

    def __init__(self, api_secret):
        self.api_secret = api_secret

    def request(self, methods, params, http_method='GET', format='json'):
        """
            methods - List of methods to be joined, e.g. ['events', 'properties', 'values']
                      will give us http://mixpanel.com/api/2.0/events/properties/values/
            params - Extra parameters associated with method
        """

        request_url = '/'.join([self.ENDPOINT, str(self.VERSION)] + methods)
        if http_method == 'GET':
            data = None
            request_url = request_url + '/?' + self.unicode_urlencode(params)
        else:
            data = self.unicode_urlencode(params)

        auth = base64.b64encode(self.api_secret).decode("ascii") #added.encode() after se
        headers = {'Authorization': 'Basic {encoded_secret}'.format(encoded_secret=auth)}
        request = urllib.request.Request(request_url, data, headers)
        response = urllib.request.urlopen(request, timeout=1000)
        str_response = response.read().decode('utf8')
        lines = str_response.splitlines(True)
        records = []
        for line in lines:
            obj = json.loads(line)
            records.append(obj)
        return records[0]

    def unicode_urlencode(self, params):
        """
            Convert lists to JSON encoded strings, and correctly handle any
            unicode URL parameters.
        """
        if isinstance(params, dict):
            params = list(params.items())
        for i,param in enumerate(params):
            if isinstance(param[1], list):
                params.remove(param)
                params.append ((param[0], json.dumps(param[1]),))

        return urllib.parse.urlencode(
            [(k, v) for k, v in params]
        )
        
    def format_params(self,event,params): #parameters here refer to API_GENERIC_PARAMETERS or whatever
        #to get results from api
        res = {
            'event': event,
            'from_date':params[0],
            'to_date':  params[1],
            'where':    params[2], 
            'type':     params[3],
            'unit':     params[4],
        }
        return res

    def format_params_engagement_numbers(self,params): #parameters here refer to API_GENERIC_PARAMETERS or whatever
        #to get results from api
        res = {
            'where':    params[0],
            #'limit': 5
        }
        return res

if __name__ == '__main__':
    encoded_secret = b'cc71690d244a6afb1279cd5cb50fc9fa'
    api = Mixpanel(api_secret=encoded_secret)
   
    '''
    data = api.request(['export'], { #uses a different endpoint
        #'event': ['%24custom_event%3A381510'],
        'event':['table view'],
        'to_date': "2018-01-16",
        'from_date': "2018-01-16",
        #'verbose':1
        #'where': 'user["userState"] == "paid"'
        #'where':'user["hasNycFeatures"] == false'
        
    })
    '''
    
    data = api.request(['events'],{
        'event':['$custom_event:381510'],
        #'event':['export'],
        'from_date':'2017-06-01',#from_date
        'to_date':'2018-01-31',#to_date
        'where':'user["hasNycFeatures"] == false and user["userIsInternal"] == false and properties["hasNationalFeatures"] == true and user["subscriptionActive"] == true and not "trialing" in user["subscriptionState"]',  #where
        'type':'unique',  #type
        'unit':'month' #unit
    })

    
    
    
    print (json.dumps(data, indent=4))