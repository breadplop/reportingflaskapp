'''
mixpanel_client2 is unable to parse json for the 'engage' method properly. 
To avoid wasting time, im using the request package instead
'''

import requests
import os
import json
import pandas as pd
import datetime
MIXPANEL_SECRET = os.environ.get('MIXPANEL_SECRET')

#url = "https://mixpanel.com/api/2.0/engage"

#querystring = {"where":"user[\"hasNycFeatures\"] == false and user[\"userIsInternal\"] == false and properties[\"hasNationalFeatures\"] == true and user[\"subscriptionActive\"] == true and not \"trialing\" in user[\"subscriptionState\"]"}
#response = requests.request("GET", url, params=querystring, auth=(MIXPANEL_SECRET,''))

#print(response.text)
#res_json = json.loads(response.text)
#print(res_json['total'])

class GetQueryString(object):
    '''
    to get query string for last seen 1 week ago, last seen 2 weeks ago, last seen 3 weeks ago
    '''
    def __init__(self):
        self.three_weeks_ago = 'properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["userIsInternal"] == false and datetime(1525975978 - 1814400) > properties["$last_seen"] and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"]'
        self.two_weeks_ago = 'properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["userIsInternal"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime(1525979138 - 1209600) > properties["$last_seen"] and datetime(1525979138 - 1814400) < properties["$last_seen"] and datetime(1525979138) >= properties["$last_seen"]'
        self.one_week_ago = 'properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["userIsInternal"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime(1525979109 - 604800) > properties["$last_seen"] and datetime(1525979109 - 1209600) < properties["$last_seen"] and datetime(1525979109) >= properties["$last_seen"]'
        self.less_than_one_week = 'properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["userIsInternal"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime(1525981271 - 604800) < properties["$last_seen"] and datetime(1525981271) >= properties["$last_seen"]'

    def get_query(self,num):
        if num == 0: query = self.less_than_one_week
        if num == 1: query =  self.one_week_ago
        if num == 2: query =  self.two_weeks_ago
        if num == 3: query = self.three_weeks_ago
        return {"where": query}

class GetLastSeenDataFromMixpanel(GetQueryString):
    '''
    to get last seen engagement data. results stored in self.all_res
    '''
    def __init__(self, file):
        self.queries = GetQueryString()
        self.dict_of_results()
        self.add_to_csv(file)
    
    def get_num_of_users_for_query(self, num):
        url = "https://mixpanel.com/api/2.0/engage"
        querystring = self.queries.get_query(num)
        response = requests.request("GET", url, params=querystring, auth=(MIXPANEL_SECRET,''))
        res_json = json.loads(response.text)
        total = res_json['total']
        return total

    def dict_of_results(self):
        list_of_results = []
        total_users = 0
        for num in range(0,4):
            num_users = self.get_num_of_users_for_query(num)
            list_of_results.append(num_users)
            total_users += num_users
        self.all_res_dict = {'<1 Week':list_of_results[0], '1-2 Weeks':list_of_results[1], '2-3 Weeks':list_of_results[2], '>3 Weeks':list_of_results[3]}    
        list_of_results.append(total_users)
        self.all_res_list = list_of_results

    def add_to_csv(self, file):
        curr_df = pd.read_csv(file, index_col=0)
        today_tuple = datetime.datetime.today().isocalendar()
        current_isoWeekNum = str(today_tuple[0]) + '-' + str(today_tuple[1])
        if str(current_isoWeekNum) in curr_df.columns:
            pass
            print("data alrdy inside")
        else:
            print(self.all_res_list)
            curr_df[current_isoWeekNum] = self.all_res_list
            curr_df.to_csv(file)
        self.table_as_html = curr_df.to_html(classes="w3-table w3-striped w3-white w3-row-padding")#border="2", classes="sortable dataframe w3-table w3-striped w3-white w3-row-padding")
    
    def get_html(self):
        return self.table_as_html


class PlotLastSeenData(GetLastSeenDataFromMixpanel):
    pass

if __name__ == '__main__':
    filename = '/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/static/data/last_seen_engagement_data.csv'
    obj = GetLastSeenDataFromMixpanel(filename)
    #pd.dataframe.to_html
    print(obj.all_res_dict)



