import numpy as np
import pandas as pd
#import urllib2
import config

import pythonscripts.mixpanel.mixpanel_client as m
import pythonscripts.mixpanel.mixpanel_dets as dets
#import mixpanel_client as m
#import mixpanel_dets as dets
import pprint
import json
import datetime
import os

print ("importing functions.py now...")

m = m.Mixpanel(config.secret)

###########################################TO GET WAUMAUDAU TABLES################################
data_dict = m.request(['events'],m.format_params(['$custom_event:381510'],dets.params_mauwaudau_unique_month_nat))
data_json = json.dumps(data_dict)

def get_mauwaudau_from_mixpanel(params): #return as column, values
    data_dict = m.request(['events'],m.format_params(['$custom_event:381510'],params))
    data_json = json.loads(json.dumps(data_dict))
    cols = data_json['data']['series']
    values = data_json['data']['values']['$custom_event:381510'] #returns as a dictionary with date and value
    return list(cols), values

def helper_consol_dates(time_period, pairs, startMonth, startYear):
    #not done because i haven removed the weekend stuff o.o 
    dictionary = {}

    if time_period == 'month':
        for k,v in pairs.items():
            y,m,d = k[0:4], k[5:7],k[8:]
            dictionary[y + '-' + m] = v

    if time_period == 'day':
        for k,v in pairs.items():
            y,m,d = k[0:4], k[5:7],k[8:]
            dateObject = datetime.date(int(y),int(m),int(d))
            if (dateObject.weekday()<5): #if weekday, add into dictionary
                if m in dictionary:
                    dictionary[y + '-' + m] = [dictionary[y + '-' + m][0] + v, dictionary[y + '-' + m][1] + 1] 
                else:
                    dictionary[y + '-' + m] = [v,1] #value, count
            else: continue #if weekend continue

        for x in dictionary: #get average
            dictionary[x] = dictionary[x][0]/dictionary[x][1]
    
    if time_period == 'week':
        week_dict = {}
        for k,v in pairs.items():
            y,m,d = k[0:4], k[5:7],k[8:]
            
            if (m < startMonth and y<= startYear): continue
            
            else:
                weeknum = datetime.date(int(y), int(m), int(d)).isocalendar()[1]
                if weeknum in week_dict:
                    week_dict[weeknum] = [week_dict[weeknum][0]+v,y + '-' + m]
                else: 
                    week_dict[weeknum] = [v,y + '-' + m]
        
        for x in week_dict:
            total, month = week_dict[x][0],week_dict[x][1]
            if month in dictionary:
                dictionary[month] = [dictionary[month][0] + total, dictionary[month][1] + 1]
            else:
                dictionary[month] = [total, 1]
        
        for y in dictionary:
            dictionary[y] = dictionary[y][0]/dictionary[y][1]
        #stopped here, seems weird
        #okay now i got the neccessary values? but not v accurate sien. need to work on this still.
    return dictionary, list(dictionary.keys())

#can delete the next 3 functions after this

def get_data_from_mixpanel(params_mau, params_wau, params_dau): #manipulate to get as an array
    _,raw_mau = get_mauwaudau_from_mixpanel(params_mau)
    _,raw_wau= get_mauwaudau_from_mixpanel(params_wau)
    _,raw_dau  = get_mauwaudau_from_mixpanel(params_dau) 
    startMonth = params_mau[0][5:7]
    startYear = params_mau[0][0:4]

    #function to remove weekends
    mau_dict, _ = helper_consol_dates('month', raw_mau, startMonth, startYear)
    wau_dict, _ = helper_consol_dates('week', raw_wau, startMonth, startYear)
    dau_dict, cols = helper_consol_dates('day', raw_dau, startMonth, startYear)

    mau_dict_values = [i for i in mau_dict.values()]
    wau_dict_values = [x for x in wau_dict.values()]
    dau_dict_values = [y for y in dau_dict.values()]
    print('mau_dict_values length is', len(mau_dict_values))
    print('wau_dict_values length is', len(wau_dict_values))
    print('dau_dict_values length is', len(dau_dict_values))
    data_array = np.array([mau_dict_values,wau_dict_values,dau_dict_values])
    #data_array = np.array([list(mau_dict.values()),list(wau_dict.values()), list(dau_dict.values())])

    return data_array, cols



def table_as_df(table_array,cols):
    table_row = ['MAU', 'WAU', 'DAU']
    table_df = pd.DataFrame(table_array, columns = cols, index = table_row)
    cols.sort()
    sort_tabled_df = table_df[list(cols)]
    

    #its not working. the sorting is not working, it's wrong

    return sort_tabled_df

def get_ratio_table(newyork, national, both, cols):
    combined = np.array(newyork) + np.array(national) + np.array(both) #combination of dau/wau/mau
    #get combined table
    combined_table_row = ['MAU', 'WAU', 'DAU']
    combined_table_col = list(cols)
    print(combined_table_col)
    print(combined)
    print(len(combined))
    print(combined[0])
    print(combined[1])
    print ('newyork')
    print(type(newyork))
    print(np.array(newyork))
    combined_table_df = pd.DataFrame(combined, columns = combined_table_col, index = combined_table_row)
    combined_table_col.sort()
    combined_sort_table_df = combined_table_df[combined_table_col]
    np.seterr(divide='ignore', invalid='ignore')
    c_wau_mau = combined[1].astype(float)/combined[0].astype(float)
    c_dau_mau = combined[2].astype(float)/combined[0].astype(float)
    ratio_table = np.concatenate(([c_wau_mau],[c_dau_mau]),axis=0).round(2)
    ratio_table_row = ('WAU/MAU','DAU/WAU')
    ratio_table_col = list(cols)

    #dataframe
    ratio_table_df = pd.DataFrame(ratio_table,
                                    columns=ratio_table_col,
                                    index = ratio_table_row)
    ratio_table_col.sort()                                   
    sort_ratio_table_df = ratio_table_df[ratio_table_col]

    #convert to percentage
    for n in sort_ratio_table_df:
        sort_ratio_table_df[n] = sort_ratio_table_df[n].map(lambda n:'{:,.1%}'.format(n))

    return sort_ratio_table_df, combined_sort_table_df

def get_df_to_html(table):
    return pd.DataFrame.to_html(table, classes = "w3-table w3-striped w3-white", index=True)

################################################## TO GET RETENTION/ENGAGEMENT DATA ############################
def get_engagement_ppl_list_from_mixpanel(params): #return as column, values
    data_dict = m.request(['engage'],m.format_params_engagement_numbers(params))
    data_json = json.loads(json.dumps(data_dict))
    return data_json['total']

def add_engagement_to_dataset_return_df(first,second,third,beyond,_all, segment):
    retrieved = False
    segment_fileConvention = segment.replace('/','')
    file_path = '/Users/brenda/Documents/Adhoc/dashreport/dashy/report/retentionmetrics_' + segment_fileConvention + '.csv'
    if os.path.isfile(file_path): #if file exists
        dataset = pd.read_csv(file_path)
    else: #if it doesnt exist, create one.
        dataset = pd.read_csv('/Users/brenda/Documents/Adhoc/dashreport/dashy/report/retentionmetrics_template.csv')
    today = datetime.date.today()
    weeknum = str(today.isocalendar()[1])
    toAdd = [today, first,second,third,beyond,float(_all)]

    if weeknum in dataset:  #doesnt work. wanted to use this to avoid adding replicates.
        retrieved = True
        print ("weeknum is already in dataset")
    else: 
        dataset[weeknum] = toAdd
    
    #dataset = dataset.set_index('Weeknum')
    dataset.to_csv('/Users/brenda/Documents/Adhoc/dashreport/dashy/report/retentionmetrics_' + segment_fileConvention + '.csv', index=False) #updated
    return dataset, retrieved

def convert_engagement_data_to_percentage(dataset, segment): #uncompleted
    headers_as_list = list(dataset.columns.values)
    dataset_array = np.array(dataset)
    dataset_array_percent = np.array(dataset_array)
    segment_fileConvention = segment.replace('/','')

    sumlast = dataset_array[-1,1:] #create a param, if weeknum alrdy in dataset, dunid to recalc percentage aagin. if nth change, dunid to create string again. 
    #replace zeroe with 1 to avoid zero division error
    np.place(sumlast, sumlast==0, 1)
    res = dataset_array_percent[1:,1:].astype(float)/sumlast.astype(float)
    res = np.around(res, decimals=3)#np.around(res.astype(np.double),3)

    for nrow in range(1,len(dataset_array_percent)):
        for ncol in range(1,len(dataset_array_percent[nrow])):
            dataset_array_percent[nrow,ncol] = '{:,.1%}'.format(res[nrow-1,ncol-1])#round(res[nrow-1,ncol-1],3)

    pd.DataFrame(dataset_array_percent, columns=headers_as_list).to_csv('/Users/brenda/Documents/Adhoc/dashreport/dashy/report/retentionmetrics_percent_' + segment_fileConvention + '.csv', index=False)
    dataset_df_percent = pd.read_csv('/Users/brenda/Documents/Adhoc/dashreport/dashy/report/retentionmetrics_percent_' + segment_fileConvention + '.csv')

    return dataset_df_percent

def changeParamsForSegment(params, segment='All'):
    if (segment=='All'): 
        pass
    else:
        #print ("params for "+segment+" is: {}".format(params))
        params[0] = params[0] + '''and ("''' + segment + '''" in properties["userSegment"]) and (defined (properties["userSegment"]))'''
    return params
'''
Functions to write:

[ ] retention chart to split by segment

'''