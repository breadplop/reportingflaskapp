#!/usr/bin/env python
"""
=====================================
HELPER FOR FEATURE AUDIT (feature_audit_tabulate2.py): MIXPANEL CLIENT
=====================================
:Author: Brenda    
:Date: 2018-04-26
Preface:
    To retrieve raw mixpanel data with @class MixpanelAPIRequest
    - to get the data you need to 
        1. form a connection with the api with the right endpoint @class Mixpanel
        2. determine parameters to get data you want @class APIRequestParams
    - @class APIRequestParams helps to set up the parameters in the appropriate format [dictionary]
        - 4 main parameters required for feature audit: list of events, from date, to date, where, type, unit
            (for more information https://mixpanel.com/help/reference/data-export-api#events)
        - @class WhereParameter helps to set up the `where` parameter (it's a v long param)
    - @class Mixpanel talks to Mixpanel API 
To improve: 
    move API Key in @class MixpanelAPIRequest to config file. gitignore.
"""
import base64
import urllib.request
import os
MIXPANEL_SECRET = os.environ.get('MIXPANEL_SECRET')
mixpanel_secret = MIXPANEL_SECRET.encode()

try:
    import json
except ImportError:
    import simplejson as json

class Mixpanel(object):
    #ENDPOINT = 'https://data.mixpanel.com/api' #diff types of data
    ENDPOINT = 'https://mixpanel.com/api'
    VERSION = '2.0'

    def __init__(self, api_secret):
        self.api_secret = api_secret

    def request(self, methods, params, http_method='GET', format='json'):
        """
            methods - List of methods to be joined, e.g. ['events', 'properties', 'values']
                      will give us http://mixpanel.com/api/2.0/events/properties/values/
            params - Extra parameters associated with method
        """

        request_url = '/'.join([self.ENDPOINT, str(self.VERSION)] + methods)
        if http_method == 'GET':
            data = None
            request_url = request_url + '/?' + self.unicode_urlencode(params)
        else:
            data = self.unicode_urlencode(params)

        auth = base64.b64encode(self.api_secret).decode("ascii") #added.encode() after se
        headers = {'Authorization': 'Basic {encoded_secret}'.format(encoded_secret=auth)}
        request = urllib.request.Request(request_url, data, headers)
        response = urllib.request.urlopen(request, timeout=1000)
        str_response = response.read().decode('utf8')
        lines = str_response.splitlines(True)
        records = []
        for line in lines:
            obj = json.loads(line)
            records.append(obj)
        return records[0] 

    def unicode_urlencode(self, params):
        """
            Convert lists to JSON encoded strings, and correctly handle any
            unicode URL parameters.
        """
        if isinstance(params, dict):
            params = list(params.items())
        for i,param in enumerate(params):
            if isinstance(param[1], list):
                params.remove(param)
                params.append ((param[0], json.dumps(param[1]),))

        return urllib.parse.urlencode(
            [(k, v) for k, v in params]
        )
        
class APIRequestParams(object):
    def __init__(self, request_type, **kwargs): 
        self.request_type = [request_type]
        self.params = {}
        self.secondinit(**kwargs)
    
    def __str__(self):
        return self.request_type + self.params

    def secondinit(self, **kwargs):

        for key, value in kwargs.items():
            self.params[key] = value
            #print("The value of {} is {}".format(key, value))
    
    def returnTuple(self):
        return (self.request_type, self.params)

class MixpanelAPIRequest(APIRequestParams):
    def __init__(self, request_type, **kwargs):
        encoded_secret = mixpanel_secret
        self.api = Mixpanel(api_secret=encoded_secret) 
        super().__init__(request_type, **kwargs)
        self.data = self.api.request(self.request_type, self.params) #json type
    
    def __str__(self):
        return self.data
    
    def get_JSONdata(self):
        return self.data

class WhereParameter(object):
    def __init__(self, hasNycFeatures=False, userIsInternal=False, hasNationalFeatures=True, subscriptionActive=True, segment=None):
        self.param_as_dict = {'user["hasNycFeatures"]':hasNycFeatures, 
        'user["userIsInternal"]':userIsInternal, 'properties["hasNationalFeatures"]': hasNationalFeatures,
        'user["subscriptionActive"]': str(subscriptionActive).lower() + ' and not "trialing" in user["subscriptionState"]', 'user["userSegment"]':'\"' + segment +'\"' if segment!=None else None}
        self.param_as_string = ''
        self.param_as_list = []
        self.create_param_string()
        self.segment = segment if segment != None else 'All' 

    def __str__(self):
        return self.param_as_string
    
    def create_param_string(self):
        for key,value in self.param_as_dict.items():
            if value is not None:                
                self.param_as_list.append([key,value])
        
        for items in self.param_as_list:
            self.param_as_string += ' ' + items[0] + ' == ' + str(items[1]).lower() + ' and '

        self.param_as_string = self.param_as_string[:-5] #remove ' and '


"""
========================================================================================================

"""

if __name__ == '__main__':
    encoded_secret = mixpanel_secret 
    api = Mixpanel(api_secret=encoded_secret)

    where =WhereParameter()
    test = APIRequestParams('events',event=['$custom_event:381510'], from_date='2017-06-01', to_date = '2018-01-31', where = where,type='average', unit='month')
    data = MixpanelAPIRequest('events',event=['$custom_event:381510'], from_date='2017-06-01', to_date = '2018-01-31', where = where,type='average', unit='month').data

    #data = api.request(*test.returnTuple())
    print (json.dumps(data, indent=4))
    print (where.param_as_string)
    