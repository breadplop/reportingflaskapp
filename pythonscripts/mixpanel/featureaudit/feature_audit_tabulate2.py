#!/usr/bin/env python
"""
=====================================
REONOMY PRODUCT ANALYTICS: FEATURE AUDIT
=====================================
:Author: Brenda    
:Date: 2018-04-26
Data Sources:
    - Mixpanel
Preface:
    Create feature audit charts from Mixpanel data
    1. Retrieve raw mixpanel data with Mixpanel Client (@class MixpanelAPIRequest)
    2. Clean/Preprocess data using numpy, pandas (@class FeatureAuditData)
    3. Plot feature audit charts with plotly (@class PlotFeatureAudit with help from @CreateScatterTrace, @CreateLayout)
"""
#from mixpanel_client2 import *
from pythonscripts.mixpanel.featureaudit.mixpanel_client2 import *
import numpy as np
import pandas as pd
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
#----------------------------start of mixpanel details -------------------------------------
import time
import os

months = {'1':'January','2':'February','3':'March','4':'April','5':'May','6':'June','7':'July','8':'August','9':'September','10':'October','11':'November','12':'December'}

class MasterList(object):
    def __init__(self, choose_all_events_bool=True):
        self.EVENTS_MASTERLIST = {
                '$custom_event:780945' : 'viewSales',
                '$custom_event:780949' : 'viewTax',
                '$custom_event:780953' : 'viewDebt',
                '$custom_event:780941' : 'viewBuilding',
                '$custom_event:743829' : 'searchSales',
                '$custom_event:743825' : 'searchOwnership',
                '$custom_event:743821' : 'searchLocation',
                '$custom_event:743817' : 'searchDebt',
                '$custom_event:743809' : 'searchCharac',
                '$custom_event:743813' : 'searchAsset',
                'table view' : 'tableView',
                'property ownership request' : 'propertyOwnershipRequest',
                'bulk contacts request' : 'bulkContactsRequest',
                'bulk label' : 'bulkLabel',
                'find comps' : 'findComps',
                'find ownership mailing address' : 'findMailingAdd',
                #'interactive map click' : 'interactiveMapClick', | returns 0, event likely to be deprecated
                'map draw click' : 'mapDrawClick',
                #'$custom_event:293376' : 'addToList', | returns 0, event likely to be deprecated
                #'$custom_event:293392' : 'listCreated', | returns 0, event likely to be deprecated
                #'parcel results list view click' : 'parcelResultsListView', | returns 0, event likely to be deprecated
                'import data' : 'importData',
                'import file' : 'importFile',
                '$custom_event:782017' : 'property_tab_view_ownership',
                'map satellite view' : 'map satellite view',
                'label property' : 'label property',
                '$custom_event:782105' : 'property_tab_view_notes',
                '$custom_event:782101' : 'property_tab_view_files',
                'address search' : 'address search',
                'map marker click' : 'map marker click',
                'map update results when moved click' : 'map update results when moved click',
                'summary card click-through' : 'summary card click-through',
                'submit search' : 'submit search',
                '$custom_event:782153' : 'all_generic_searches',
                'export' : 'export',
                '$custom_event:782185' : 'location_smart_search',
                '$custom_event:782193' : 'targeted_property_search',
                'Viewed parcel Page' : 'Viewed parcel page',
                '$custom_event:822565': 'ownershipPortfolioSearch', #searchtabview-ownership + find ownership mailing address
                '$custom_event:822557': 'advancedSearch (all except searchownership)', #searchcharacteristics,asset,debt, location
                'print property details': 'printPDF',
                'page view: search': 'searchOwnershipPortfolioFromResults'
            }

        self.ESSENTIAL_EVENTS_MASTERLIST = {
            'property ownership request' : 'propertyOwnershipRequest',
            'export' : 'export',
            'Viewed parcel Page' : 'page_link',
            'bulk contacts request' : 'bulk_contacts',
            '$custom_event:822565': 'ownershipPortfolioSearch', #searchtabview-ownership + find ownership mailing address
            '$custom_event:822557': 'sourcing', #searchcharacteristics,asset,debt, location
            'map draw click' : 'map_draw', #includes radius/polygon search
            '$custom_event:782193' : 'targeted_property_search',
            'map marker click' : 'map marker click',
            'table view' : 'tableView',
            'map satellite view' : 'map satellite view',
            'print property details': 'printPDF',
            'page view: search': 'searchOwnershipPortfolioFromResults' #click hyperlink in results page to lookup ownership portfolio
        }
        self.choose_all_events_bool = choose_all_events_bool
        self.settle_events_list()
    
    def settle_events_list(self): #return sorted_events_master_list, events_name_list, labels
        events_list_to_use = self.EVENTS_MASTERLIST if self.choose_all_events_bool else self.ESSENTIAL_EVENTS_MASTERLIST
        self.SORTED_EVENTS_MASTERLIST = sorted(events_list_to_use)#.items())
        self.EVENTS_NAME_LIST = list(events_list_to_use.keys())
        self.labels = [] #self.labels is wrong. dont use it. 
        for label in self.SORTED_EVENTS_MASTERLIST:
            #self.labels.append(label[1])
            readable_label = events_list_to_use[label] #label = label used to call from mixpanel
            # ('label = {}, readable_label = {}'.format(label, readable_label))
            self.labels.append(readable_label)

        return self.SORTED_EVENTS_MASTERLIST, self.EVENTS_NAME_LIST, self.labels

#----------------------------end of mixpanel details -------------------------------------
class FeatureAuditData(MixpanelAPIRequest, MasterList):
    def __init__(self, request_type, event, from_date, to_date, where, unit, choose_all_events_bool=True): 
        MasterList.__init__(self,choose_all_events_bool)
        self.request_type = request_type
        self.event = event
        self.from_date = from_date
        self.to_date = to_date
        self.where = where
        self.unit = unit
        self.board_data = MixpanelAPIRequest(self.request_type, event = ['$custom_event:381510'], from_date = self.from_date, to_date = self.to_date, where = self.where, type = 'unique', unit = self.unit).get_JSONdata()
        self.second_init()

    def getAdoptionValues(self):
        raw_data = MixpanelAPIRequest(self.request_type, event = self.event, from_date = self.from_date, to_date = self.to_date, where = self.where, type = 'unique', unit = self.unit).get_JSONdata()
        self.table_num_users_for_each_event = pd.DataFrame.from_dict(raw_data.copy()['data']['values'],orient='index')
        self.adoption_values_json = raw_data.copy()['data']['values'] #need to divide by board_data
        self.new_labels = []
        self.new_labels_readable = []
        for event, dict_of_time_and_values in self.adoption_values_json.items():
            self.new_labels.append(str(event))
            self.new_labels_readable.append(str(self.EVENTS_MASTERLIST[event]))
            for time, value in dict_of_time_and_values.items():
                num_board_for_event = self.board_data['data']['values']["$custom_event:381510"][time]
                self.adoption_values_json[event][time] = round(value/num_board_for_event,3) if num_board_for_event != 0 else 0

    def getFrequencyValues(self):
        raw_data = MixpanelAPIRequest(self.request_type, event = self.event, from_date = self.from_date, to_date = self.to_date, where = self.where, type = 'average', unit = self.unit).get_JSONdata()
        self.frequency_values_json = raw_data['data']['values'] #returns as dict{event1:{date1:value1,date2:value2},event2:{date1:value1,date2:value2}}

    def format_table(self): #to get a list of number of users who performed the event in each month
        #add total board number
        table_for_board_event_only = pd.DataFrame.from_dict(self.board_data.copy()['data']['values'],orient='index')
        self.table_num_users_for_each_event  = pd.concat([self.table_num_users_for_each_event,table_for_board_event_only ])
        
        table =  self.table_num_users_for_each_event.reset_index()
        for idx in range(0,len(table)):
            event_name_in_mixpanel = table['index'][idx]
            if event_name_in_mixpanel in self.EVENTS_MASTERLIST:
                table['index'][idx] = self.EVENTS_MASTERLIST[event_name_in_mixpanel]

            if event_name_in_mixpanel == '$custom_event:381510':
                table['index'][idx] = 'board'
        table=table[table.columns.sort_values(ascending=False)] #sort month columns in descending order
        table = table.sort_values(table.columns[0], ascending=False)  #sort the most recent column (first column) in descending order
        self.table_num_users_for_each_event_as_html = table.to_html(border="2", classes="sortable dataframe w3-table w3-striped w3-white w3-row-padding")

    def second_init(self):
        self.months_as_list = list(self.board_data['data']['series'])
        self.getAdoptionValues()
        self.getFrequencyValues()
        self.format_table()

class PlotFeatureAudit(FeatureAuditData):
    def __init__(self, request_type, event, from_date, to_date, where, unit, choose_all_events_bool, slideshow = False): 
        super().__init__(request_type, event, from_date, to_date, where, unit, choose_all_events_bool)
        self.slideshow = slideshow
        self.secondinit()
    
    def secondinit(self):
        self.create_plot_layout()
        if self.slideshow: 
            self.create_indv_plots_for_indv_months()
        else:
            self.create_plot_all_traces(visible_month=self.months_as_list[-1])
            self.create_plot_html()

    def create_plot_all_traces(self,visible_month=''):
        self.all_traces = []
        self.dict_storing_adop_freq_values_for_each_month = {}
        for month in self.months_as_list:
            adoption_for_month = []
            frequency_for_month = []
            for event, dict_of_time_and_values in self.adoption_values_json.items():
                adoption_for_month.append(dict_of_time_and_values[month])

            for event, dict_of_time_and_values in self.frequency_values_json.items():
                frequency_for_month.append(dict_of_time_and_values[month])

            visible = "true" if month==visible_month else "legendonly"
            #idv_trace_for_month = CreateScatterTrace(adoption_for_month, frequency_for_month, plabels=self.labels, name=month, visible=visible).get_trace()
            idv_trace_for_month = CreateScatterTrace(adoption_for_month, frequency_for_month, plabels=self.new_labels_readable, name=month, visible=visible).get_trace()
            self.dict_storing_adop_freq_values_for_each_month[month] = {'adoption': adoption_for_month, 'frequency':frequency_for_month} #to calc difference btwn months in plot_differnece()
            
            if month == visible_month and self.slideshow==True:#can delete i think
                self.temp_trace_for_month = [idv_trace_for_month]
            elif self.slideshow!=True:
                self.all_traces.append(idv_trace_for_month) 

    def create_plot_layout(self):
        segment = self.where.segment
        self.layout = CreateLayout(segment=segment).get_layout()

    def create_plot_html(self,traces=""):
        if self.slideshow==True:
            data = traces
            filename = 'featureaudit_indvmonth.html'
            fig = go.Figure(data=data, layout = self.layout)
            self.temp_filename_for_month = plotly.offline.plot(fig, show_link=False, filename=filename, auto_open=False,include_plotlyjs=False,output_type='div')
        else:
            data = self.all_traces
            filename = 'featureaudit.html'
            fig = go.Figure(data=data, layout = self.layout)
            self.filename = plotly.offline.plot(fig, show_link=False, filename=filename, auto_open=False,include_plotlyjs=False,output_type='div')

    def create_indv_plots_for_indv_months(self):
        #print ("function create_indv_plots_for_indv_months is taking place")
        for month in self.months_as_list:
            #print("month is ", month)
            self.create_plot_all_traces(visible_month=month)
            self.create_plot_html(traces = self.temp_trace_for_month)

class CreateScatterTrace():
    def __init__(self, adoption, frequency, plabels, name = 'All',visible = 'legendonly'): 
        self.trace = go.Scatter(
            x = adoption,
            y = frequency,
            mode = 'markers+text',
            text = plabels,
            textposition = 'bottom',
            name = name,
            visible = visible
        )
    
    def get_trace(self): return self.trace

class CreateLayout():
    def __init__(self, segment = 'All', specialBool=False, month1='', month2=''):
        if not specialBool:
            self.title = 'Feature Audit for ' + segment
            self.showlegend = True

        else:
            self.title = 'Difference between ' + month1 + ' and ' + month2 + ' for ' + segment
            self.showlegend = False

        self.set_layout()

    def set_layout(self):
        self.layout= go.Layout(
            title= self.title,
            hovermode= 'closest',
            xaxis= dict(
                title= 'Adoption',
                ticklen= 5,
                zeroline= False,
                gridwidth= 2,
            ),
            yaxis=dict(
                title= 'Frequency',
                ticklen= 5,
                gridwidth= 2,
            ),
            showlegend= self.showlegend
        )

    def get_layout(self): return self.layout

################################ Plot Default Feature Audit Chart ###############################
# --------------- from june to jan
class PlotDefault():
    def __init__(self, from_date='2017-08-01', to_date = '2018-04-30', where = WhereParameter(), unit='month', choose_all_events_bool=True, slideshow=False):
        event = MasterList(choose_all_events_bool).EVENTS_NAME_LIST
        self.chart_object = PlotFeatureAudit('events',event=event, from_date=from_date, to_date = to_date, where = where, unit=unit, slideshow=slideshow, choose_all_events_bool=choose_all_events_bool)
        self.chart_html_file = self.chart_object.filename
        self.adoption_values_json = self.chart_object.adoption_values_json
        self.frequency_values_json = self.chart_object.frequency_values_json
        self.table_num_users_for_each_event_as_html = self.chart_object.table_num_users_for_each_event_as_html

    def plot_difference(self, month1, month2, segment=""):
        all_values = self.chart_object.dict_storing_adop_freq_values_for_each_month
        month1_values, month2_values = all_values[month1], all_values[month2]
        filename = "diff btwn" + month1 + "and" + month2
        diff_adoption = np.array(month2_values['adoption']) - np.array(month1_values['adoption'])
        diff_frequency = np.array(month2_values['frequency']) - np.array(month1_values['frequency'])
        trace = CreateScatterTrace(diff_adoption, diff_frequency, plabels=self.chart_object.labels, name=filename, visible="true").get_trace()
        layout = CreateLayout(segment,True, month1,month2).get_layout()

        fig = go.Figure(data=[trace], layout = layout)
        self.feature_audit_comparison_chart = plotly.offline.plot(fig, show_link=False, filename=filename, auto_open=False,include_plotlyjs=False,output_type='div')

        return self.feature_audit_comparison_chart
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
if __name__ == '__main__': 
    #stopped here. not sure why this only works for service provider
    #even then, value is wrong. this is weird. 
    print(where2.param_as_string)
    ##chart = PlotFeatureAudit('events',event=MasterList(True).EVENTS_NAME_LIST, from_date='2017-06-01', to_date = '2018-01-31', where = where, unit='month', slideshow=False)

    #if you want to plot individual months: PlotFeatureAudit(slideshow=True). filenames/htmlfile are in instance.filenames_in_list_idv_month_slideshow as a list
    #if you want to print all months: PlotFeatureAudit(slideshow=False). filenames/htmlfile is in instance.filename