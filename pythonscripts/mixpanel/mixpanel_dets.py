#mixpanel details
import time
import os

print ("importing mixpanel details now...")

months = {'1':'January','2':'February','3':'March','4':'April','5':'May','6':'June','7':'July','8':'August','9':'September','10':'October','11':'November','12':'December'}

EVENTS_MASTERLIST = {
        '$custom_event:780945' : 'viewSales',
        '$custom_event:780949' : 'viewTax',
        '$custom_event:780953' : 'viewDebt',
        '$custom_event:780941' : 'viewBuilding',
        '$custom_event:743829' : 'searchSales',
        '$custom_event:743825' : 'searchOwnership',
        '$custom_event:743821' : 'searchLocation',
        '$custom_event:743817' : 'searchDebt',
        '$custom_event:743809' : 'searchCharac',
        '$custom_event:743813' : 'searchAsset',
        'table view' : 'tableView',
        'property ownership request' : 'propertyOwnershipRequest',
        'bulk contacts request' : 'bulkContactsRequest',
        'bulk label' : 'bulkLabel',
        'find comps' : 'findComps',
        'find ownership mailing address' : 'findMailingAdd',
        'interactive map click' : 'interactiveMapClick',
        'map draw click' : 'mapDrawClick',
        '$custom_event:293376' : 'addToList',
        '$custom_event:293392' : 'listCreated',
        'parcel results list view click' : 'parcelResultsListView',
        'import data' : 'importData',
        'import file' : 'importFile',
        '$custom_event:782017' : 'property_tab_view_ownership',
        'map satellite view' : 'map satellite view',
        'label property' : 'label property',
        'custom_event:782105' : 'property_tab_view_notes',
        'custom_event:782101' : 'property_tab_view_files',
        'address search' : 'address search',
        'map marker click' : 'map marker click',
        'map update result when moved click' : 'map update result when moved click',
        'summary card click-through' : 'summary card click-through',
        'submit search' : 'submit search',
        'custom_event:782153' : 'all_generic_searches',
        'export' : 'export',
        'custom_event:782185' : 'location_smart_search',
        'custom_event:782193' : 'targeted_property_search',
        'Viewed parcel page' : 'Viewed parcel page',
        '$custom_event:822565': 'ownershipPortfolioSearch', #searchtabview-ownership + find ownership mailing address
        '$custom_event:822557': 'advancedSearch (all except searchownership)', #searchcharacteristics,asset,debt, location
        'print property details': 'printPDF'
    }


SORTED_EVENTS_MASTERLIST = sorted(EVENTS_MASTERLIST.items())
EVENTS_NAME_LIST = EVENTS_MASTERLIST.keys()

#labels
labels = []
for label in SORTED_EVENTS_MASTERLIST:
    labels.append(label[1])

#PARAMETERS
####################### NATIONAL #######################
params_mauwaudau_unique_month_nat = [
        '2017-06-01',#from_date
        '2018-01-31',#to_date
        'user["hasNycFeatures"] == false and user["userIsInternal"] == false and properties["hasNationalFeatures"] == true and user["subscriptionActive"] == true and not "trialing" in user["subscriptionState"]',  #where
        'unique',  #type
        'month' #unit
    ]
    
params_mauwaudau_unique_week_nat = list(params_mauwaudau_unique_month_nat)
params_mauwaudau_unique_day_nat = list(params_mauwaudau_unique_month_nat)
params_mauwaudau_unique_week_nat[4] = 'week'
params_mauwaudau_unique_day_nat[4] = 'day'
####################### NEW YORK #######################
params_mauwaudau_unique_month_ny = [
        '2017-06-01',#from_date
        '2018-01-31',#to_date
        'user["hasNycFeatures"] == true and user["userIsInternal"] == false and properties["hasNationalFeatures"] == false and user["subscriptionActive"] == true and not "trialing" in user["subscriptionState"]',  #where
        'unique',  #type
        'month' #unit
    ]
    
params_mauwaudau_unique_week_ny = list(params_mauwaudau_unique_month_ny)
params_mauwaudau_unique_day_ny = list(params_mauwaudau_unique_month_ny)
params_mauwaudau_unique_week_ny[4] = 'week'
params_mauwaudau_unique_day_ny[4] = 'day'

####################### BOTH #######################
params_mauwaudau_unique_month_both = [
        '2017-06-01',#from_date
        '2018-01-31',#to_date
        'user["hasNycFeatures"] == true and user["userIsInternal"] == false and properties["hasNationalFeatures"] == true and user["subscriptionActive"] == true and not "trialing" in user["subscriptionState"]',  #where
        'unique',  #type
        'month' #unit
    ]
    
params_mauwaudau_unique_week_both = list(params_mauwaudau_unique_month_both)
params_mauwaudau_unique_day_both = list(params_mauwaudau_unique_month_both)
params_mauwaudau_unique_week_both[4] = 'week'
params_mauwaudau_unique_day_both[4] = 'day'


#################################################################################################################################
#################################################################################################################################
###########################################  STUFF FOR GETTING ENGAGEMENT METRICS   ###########################################x

########################################### PARAMETERS ###########################################
def getParamsEngagement(timePeriod):
    today_int = int(time.time())
    today_str = str(today_int)
    sevenDaysAgo_str = str(today_int - 604800)
    fourteenDaysAgo_str = str(today_int-1209600)
    twentyoneDaysAgo_str = str(today_int -1814400)

    res = []
    if timePeriod == 'all':
        res = ['(properties["userIsInternal"] == false and properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"])']
    
    if timePeriod == 'first':
        res = ['(properties["userIsInternal"] == false and properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime('+sevenDaysAgo_str+') < properties["$last_seen"] and datetime('+today_str+') >= properties["$last_seen"])']

    if timePeriod == 'second':
        res = ['(properties["userIsInternal"] == false and properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime('+sevenDaysAgo_str+') > properties["$last_seen"] and datetime('+fourteenDaysAgo_str+') < properties["$last_seen"] and datetime('+today_str+') >= properties["$last_seen"])']
    
    if timePeriod == 'third':
        res = ['(properties["userIsInternal"] == false and properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime('+fourteenDaysAgo_str+') > properties["$last_seen"] and datetime('+twentyoneDaysAgo_str+') < properties["$last_seen"] and datetime('+today_str+') >= properties["$last_seen"])']
    
    if timePeriod == 'beyond':
        res = ['(properties["userIsInternal"] == false and properties["hasNationalFeatures"] == true and properties["hasNycFeatures"] == false and properties["subscriptionActive"] == true and not "trialing" in properties["subscriptionState"] and datetime('+twentyoneDaysAgo_str+') > properties["$last_seen"])']

    return res



########################################### PARAMETERS - change segment ###########################################
segment_list = [
    'Sales Broker',
    'Service Provider',
    'Investor/Developer - Professional',
    'Other',
    'Investor/Developer - Hobbyist',
    'undefined', 
    'Research', 
    'Lender - Origination',
    'Appraiser',
    'Debt Broker',
    'Property Manager',
    'Leasing Broker',
    'Lender - Underwriting'
]


if __name__ == __name__:
    #print('\n Current start date for ny, nat and both: \n {}, {}, {} '.format(params_mauwaudau_unique_month_ny[0], params_mauwaudau_unique_month_nat[0], params_mauwaudau_unique_month_both[0]))
    #_new_start = input("Please enter start date for MAU Board Metrics. (E.g. 2017-06-01) \n")
    #print('\n Current end date for ny, nat and both: \n {}, {}, {} '.format(params_mauwaudau_unique_month_ny[1], params_mauwaudau_unique_month_nat[1], params_mauwaudau_unique_month_both[1]))
    #_new_end = input("Please enter end date for MAU Board Metrics. (E.g. 2018-02-20) \n")
    print ("mixpanel_dets.py file, need to change start and end date here... ")
    _new_start = '2017-06-01'
    _new_end = '2018-05-05'
    if (_new_start != 'no'):
        params_mauwaudau_unique_month_ny[0] = _new_start
        params_mauwaudau_unique_month_nat[0] = _new_start
        params_mauwaudau_unique_month_both[0] = _new_start

    if (_new_start != 'no'):
        params_mauwaudau_unique_month_ny[1] = _new_end
        params_mauwaudau_unique_month_nat[1] = _new_end
        params_mauwaudau_unique_month_both[1] = _new_end

    params_mauwaudau_unique_week_nat = list(params_mauwaudau_unique_month_nat)
    params_mauwaudau_unique_day_nat = list(params_mauwaudau_unique_month_nat)
    params_mauwaudau_unique_week_nat[4] = 'week'
    params_mauwaudau_unique_day_nat[4] = 'day'

    params_mauwaudau_unique_week_ny = list(params_mauwaudau_unique_month_ny)
    params_mauwaudau_unique_day_ny = list(params_mauwaudau_unique_month_ny)
    params_mauwaudau_unique_week_ny[4] = 'week'
    params_mauwaudau_unique_day_ny[4] = 'day'

    params_mauwaudau_unique_week_both = list(params_mauwaudau_unique_month_both)
    params_mauwaudau_unique_day_both = list(params_mauwaudau_unique_month_both)
    params_mauwaudau_unique_week_both[4] = 'week'
    params_mauwaudau_unique_day_both[4] = 'day'

    params_engagement_all = getParamsEngagement('all')
    params_engagement_1week = getParamsEngagement('first')
    params_engagement_2week = getParamsEngagement("second")
    params_engagement_3week = getParamsEngagement("third")
    params_engagement_beyond = getParamsEngagement("beyond")
    #print('check start date for ny, nat and both: \n {}, {}, {} \n'.format(params_mauwaudau_unique_month_ny[0], params_mauwaudau_unique_month_nat[0], params_mauwaudau_unique_month_both[0]))
    #print('check end date for ny, nat and both: \n {}, {}, {} \n'.format(params_mauwaudau_unique_month_ny[1], params_mauwaudau_unique_month_nat[1], params_mauwaudau_unique_month_both[1]))