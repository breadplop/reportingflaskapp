import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import pythonscripts.mixpanel.functions as f
import pythonscripts.mixpanel.mixpanel_dets as dets
#import functions as f
#import mixpanel_dets as dets
#Task: To create the MAU/WAU/DAU for a certain date

#master dict will contain all of the tables/charts that will be included in the html report

master = {}

################################## to get mau wau dau numbers #######################

national, _ = f.get_data_from_mixpanel(dets.params_mauwaudau_unique_month_nat,dets.params_mauwaudau_unique_week_nat,dets.params_mauwaudau_unique_day_nat)
newyork, _  = f.get_data_from_mixpanel(dets.params_mauwaudau_unique_month_ny, dets.params_mauwaudau_unique_week_ny, dets.params_mauwaudau_unique_day_ny)
both, cols = f.get_data_from_mixpanel(dets.params_mauwaudau_unique_month_both, dets.params_mauwaudau_unique_week_both, dets.params_mauwaudau_unique_day_both)


ratio_table_df, combined_sort_table_df = f.get_ratio_table(newyork,national, both, list(cols))
ratio_table_df_html = f.get_df_to_html(ratio_table_df)
combined_sort_table_df_html = f.get_df_to_html(combined_sort_table_df)
join_tables = combined_sort_table_df.append(ratio_table_df)
join_tables_html = f.get_df_to_html(join_tables)
#master['ratio_table'] = ratio_table
master['ratio_table_df'] = ratio_table_df
master['ratio_table_df_html'] = ratio_table_df_html
master['combined_sort_table_df'] = combined_sort_table_df
master['combined_sort_table_df_html'] = combined_sort_table_df_html
master['join_tables_html'] = join_tables_html

########################################
def return_master():
    return master


########################################

