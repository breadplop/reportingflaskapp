#!/usr/bin/env python
"""
=====================================
REONOMY PRODUCT ANALYTICS: STICKINESS RATIO
=====================================
:Author: Brenda    
:Date: 2018-05-01
Preface:
    To calculate stickiness ratio (mau/dau/wau) across months
    1. get data from mixpanel
    2. data cleaning and preprocessing
        - remove weekends and public holidays
    3. calculate stickiness ratio
To improve: 
    
"""

import numpy as np
import matplotlib
matplotlib.use('Agg')
import pandas as pd
#plt = matplotlib.pyplot
import matplotlib.pyplot as plt
import datetime
from pythonscripts.mixpanel.featureaudit.mixpanel_client2 import MixpanelAPIRequest, WhereParameter

class GetCleanedDataForSingleUnit(object): #self.cleaned_dict
    '''
    Step 1: get raw data with MixpanelAPIRequest class (board_data_raw)
    Step 2: clean (remove weekends, sum numbers for similar months) (cleaned_dict)
    '''
    def __init__(self, unit, from_date='2017-08-01', to_date='2018-03-31', where=WhereParameter().param_as_string, single_event = '$custom_event:381510'):
        self.from_date = from_date
        self.to_date = to_date
        self.where = where
        self.unit = unit
        self.board_data_raw = MixpanelAPIRequest('events', event = [single_event], from_date = self.from_date, to_date = self.to_date, where = self.where, type = 'unique', unit = unit).get_JSONdata()
        self.dict_with_date_n_value = self.board_data_raw['data']['values'][single_event]
        self.helper_consol_dates() #creates self.cleaned_dict

    #removes weekends, sum numbers acc to month
    def helper_consol_dates(self):
        #time_period, pairs, startMonth, startYear
        time_period, pairs, startMonth, startYear = self.unit , self.dict_with_date_n_value , self.from_date[5:7], self.from_date[0:4]
        dictionary = {}

        if time_period == 'month':
            for k,v in pairs.items():
                y,m,d = k[0:4], k[5:7],k[8:]
                dictionary[y + '-' + m] = v

        if time_period == 'day':
            for k,v in pairs.items():
                y,m,d = k[0:4], k[5:7],k[8:]
                dateObject = datetime.date(int(y),int(m),int(d))
                if (dateObject.weekday()<5): #if weekday, add into dictionary
                    if m in dictionary:
                        dictionary[y + '-' + m] = [dictionary[y + '-' + m][0] + v, dictionary[y + '-' + m][1] + 1] 
                    else:
                        dictionary[y + '-' + m] = [v,1] #value, count
                else: continue #if weekend continue running through the list

            for x in dictionary: #get average
                dictionary[x] = dictionary[x][0]/dictionary[x][1]
                print('for day, average for month {} : {} , where: {}'.format(x, dictionary[x], self.where))
        
        if time_period == 'week':
            week_dict = {}
            for k,v in pairs.items():
                y,m,d = k[0:4], k[5:7],k[8:]
                
                if (m < startMonth and y<= startYear): continue
                
                else:
                    weeknum = datetime.date(int(y), int(m), int(d)).isocalendar()[1]
                    if weeknum in week_dict:
                        week_dict[weeknum] = [week_dict[weeknum][0]+v,y + '-' + m]
                    else: 
                        week_dict[weeknum] = [v,y + '-' + m]
            
            for x in week_dict:
                total, month = week_dict[x][0],week_dict[x][1]
                if month in dictionary:
                    dictionary[month] = [dictionary[month][0] + total, dictionary[month][1] + 1]
                else:
                    dictionary[month] = [total, 1]
            
            for y in dictionary:
                dictionary[y] = dictionary[y][0]/dictionary[y][1]
            #stopped here, seems weird
            #okay now i got the neccessary values? but not v accurate sien. need to work on this still.
        
        self.cleaned_dict = dictionary
            

class GetCleanedMAUWAUDAUData(object):
    '''
    get cleaned data for month, week and day using @class GetCleanedDataForSingleUnit 
    '''
    def __init__(self, from_date='2017-08-01', to_date='2018-03-31', where=WhereParameter().param_as_string):
        self.from_date = from_date
        self.to_date = to_date
        self.where = where
        self.get_data_for_all_day_week_month()
    
    def get_data_for_all_day_week_month(self, single_event = '$custom_event:381510'):
        self.dict_with_date_n_value_month = GetCleanedDataForSingleUnit('month', self.from_date, self.to_date, self.where,single_event).cleaned_dict
        self.dict_with_date_n_value_week = GetCleanedDataForSingleUnit('week', self.from_date, self.to_date, self.where,single_event).cleaned_dict
        self.dict_with_date_n_value_day = GetCleanedDataForSingleUnit('day', self.from_date, self.to_date, self.where,single_event).cleaned_dict

    def get_data_array_to_construct_ratio_table(self):
        mau_dict = self.dict_with_date_n_value_month
        wau_dict = self.dict_with_date_n_value_week
        dau_dict = self.dict_with_date_n_value_day
        unsorted_mauwaudau_list = [mau_dict,wau_dict,dau_dict]
        sorted_mauwaudau_list = []
        temp_list = []
        for l in unsorted_mauwaudau_list:
            temp_list = [z[1] for z in sorted(l.items())]
            sorted_mauwaudau_list.append(temp_list)
        
        self.data_array_month_week_day_values = np.array(sorted_mauwaudau_list)
        return self.data_array_month_week_day_values
    
    def get_columns(self):
        return list(self.dict_with_date_n_value_month.keys()) 

class ConstructRatioTables(object):
    '''
    with cleaned_data, create ratio tables (as df and html)
    '''
    def __init__(self, from_date='2017-08-01', to_date='2018-03-31'):
        where_both = WhereParameter(hasNycFeatures=True, hasNationalFeatures=True)
        where_natl = WhereParameter(hasNycFeatures=False, hasNationalFeatures=True)
        where_ny = WhereParameter(hasNycFeatures=True, hasNationalFeatures=False)

        where_ny = 'not "trialing" in user["subscriptionState"] and properties["hasNycFeatures"] == true and properties["hasNationalFeatures"] == false and properties["userIsInternal"] == false and not "trial_ended" in user["subscriptionState"] and not "none" in user["subscriptionState"]'
        where_both = 'not "trialing" in user["subscriptionState"] and properties["hasNycFeatures"] == true and properties["hasNationalFeatures"] == true and properties["userIsInternal"] == false and not "trial_ended" in user["subscriptionState"] and not "none" in user["subscriptionState"]'
        where_natl = 'not "trialing" in user["subscriptionState"] and properties["hasNycFeatures"] == false and properties["hasNationalFeatures"] == true and properties["userIsInternal"] == false and not "trial_ended" in user["subscriptionState"] and not "none" in user["subscriptionState"]'
        self.both_obj = GetCleanedMAUWAUDAUData(from_date, to_date, where= where_both)
        self.natl_obj = GetCleanedMAUWAUDAUData(from_date, to_date, where= where_natl)
        self.ny_obj = GetCleanedMAUWAUDAUData(from_date, to_date, where= where_ny)

        self.both_data = self.both_obj.get_data_array_to_construct_ratio_table()
        self.natl_data = self.natl_obj.get_data_array_to_construct_ratio_table()
        self.ny_data = self.ny_obj.get_data_array_to_construct_ratio_table()
        self.cols = sorted(self.both_obj.get_columns())

        self.get_ratio_table() #creates self.sort_ratio_table_df, self.combined_sort_table_df

    def get_ratio_table(self):
        newyork = self.ny_data
        national = self.natl_data
        both = self.both_data
        cols = self.cols 
        combined = newyork + national + both #combination of dau/wau/mau
        print(national)
        print(both)
        print(newyork)
        print(combined)
        #get combined table
        combined_table_row = ['MAU', 'WAU', 'DAU']
        combined_table_col = list(cols)
        
        combined_table_df = pd.DataFrame(combined, columns = combined_table_col, index = combined_table_row)
        combined_table_col.sort()
        combined_sort_table_df = combined_table_df[combined_table_col]
        c_wau_mau = combined[1].astype(float)/combined[0].astype(float)
        c_dau_mau = combined[2].astype(float)/combined[0].astype(float)
        ratio_table = np.concatenate(([c_wau_mau],[c_dau_mau]),axis=0).round(2)
        ratio_table_row = ('WAU/MAU','DAU/WAU')
        ratio_table_col = list(cols)

        #dataframe
        ratio_table_df = pd.DataFrame(ratio_table,
                                        columns=ratio_table_col,
                                        index = ratio_table_row)
        ratio_table_col.sort()                                   
        sort_ratio_table_df = ratio_table_df[ratio_table_col]

        #convert to percentage
        for n in sort_ratio_table_df:
            sort_ratio_table_df[n] = sort_ratio_table_df[n].map(lambda n:'{:,.1%}'.format(n))

        self.sort_ratio_table_df, self.combined_sort_table_df = sort_ratio_table_df, combined_sort_table_df
        self.overall_table = combined_sort_table_df.append(sort_ratio_table_df)
    
    @staticmethod
    def get_html_to_df(table):
        return pd.DataFrame.to_html(table, classes = "w3-table w3-striped w3-white", index=True)

    