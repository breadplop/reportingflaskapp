#SQL QUERIES
def set_query(name = 'contact', individual = False, byplan = False, segment = ''):
        if name == 'segment_list':
                query = '''
                        SELECT distinct(settings-> 'role') as segment
                        FROM visage.user
                '''
                return query
        if 'contact' in name:
                feature_id = '\'181c5854-792c-41f2-a389-0809e37e611b\''
        if 'export' in name:
                feature_id = '\'8871e629-a8ff-4d01-8d7c-1040eb943d4d\''
        if individual == True:
                filtersegment = 'WHERE segment=\'' + segment + '\'' if segment != '' else ''
                lastquery = '''
                SELECT * FROM each_paying_seat
                ''' + filtersegment 
        else: #combine query to get quartiles
                include_plan_in_col = ''
                group_by_plan = ''

                filter_by_segment = ''

                if byplan == True:
                        include_plan_in_col = 'x.fg_name AS plan,'
                        group_by_plan = ',plan'
                
                if segment != '':
                        filter_by_segment = ' AND x.segment=\'' + str(segment) + '\''

                lastquery = '''
                                SELECT 
                                        to_char(x.feature_usage_created_month,'YYYY-MM') as month,
                                        ''' +include_plan_in_col + '''
                                        sum(used_count) AS total_used_count,
                                        sum(allowed_count) AS total_allowed_count,
                                        concat(round(sum(used_count)*100.0/sum(allowed_count),2),'%') AS percent_used,
                                        count(email) AS count_email,
                                        count(distinct(email)) AS dist_email,
                                        concat(round(CAST(percentile_cont(0.25) WITHIN GROUP (ORDER BY percent_used) as numeric),2), '%') as first_quartile,
                                        concat(round(CAST(percentile_cont(0.50) WITHIN GROUP (ORDER BY percent_used) as numeric),2), '%') as second_quartile,
                                        concat(round(CAST(percentile_cont(0.75) WITHIN GROUP (ORDER BY percent_used) as numeric),2), '%') as third_quartile
                                        FROM each_paying_seat x
                                WHERE x.fg_name NOT IN ('admin') AND x.feature_usage_created_month is not null''' + filter_by_segment + '''
                                GROUP BY month ''' + group_by_plan + '''
                                ORDER BY 1 desc
                                ;
                        '''
                
        query = '''
        WITH _only_feature AS (SELECT * FROM visage.feature_usage f WHERE feature_id = ''' + feature_id + '''),
        each_paying_seat AS 
        (SELECT 
                distinct(ss.id) AS seat_id,
                u.email,
                u.settings->'role' as segment,
                f.used_count,
                pr.allowed_count,
                f.used_count*100.0/pr.allowed_count AS percent_used,
                cast(to_char(f.created_at, 'YYYY-MM-DD') as date) AS feature_usage_created_month,
                fg.name AS fg_name,
                to_char(f.created_at,'yyyy-mm') as feature_usage_created_month_only
        FROM visage.user u
                JOIN customer.subscription_seat ss ON ss.user_id = u.id
                JOIN visage.feature_usage_period_rule pr ON pr.feature_group_id = ss.feature_group_id
                LEFT JOIN _only_feature f on f.subscription_seat_id = ss.id
                JOIN customer.feature_group fg ON fg.id = ss.feature_group_id
        WHERE 
                NOT u.is_admin AND
                lower(u.email) not like '%@reonomy.com' AND
                pr.feature_id = ''' + feature_id + ''' --unique feature
                AND ss.feature_group_id NOT IN ('f8e00c1f-5a62-43bf-93ea-815f1bce1be7')--exclude national trial
                ORDER BY 2,5 DESC
        )
        ''' + lastquery 
       
        return query


def getQuery(name='contact', segment = '', startDate=0, endDate=0): #not done
        _name, _individual, _byplan, _segment = name, False, False, segment
        
        if name == 'export by plan':
                _byplan = True
        if name == 'exporteachpayingseat' or name == 'contacteachpayingseat':
                _individual = True
        if name == 'segment_list':
                query = set_query('segment_list')

        query = set_query(_name, _individual, _byplan, _segment)
        return query

if __name__ == '__main__': #for testing purposes
        print(getQuery('contact'))

        print(getQuery('contacteachpayingseat', 'Sales Broker'))

        print(getQuery)
