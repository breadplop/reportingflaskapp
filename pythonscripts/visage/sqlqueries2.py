
class BaseSQLQuery(object):
    '''
    self._select
    self._from
    self._where
    self._order
    self._feature #default is contact
    self.feature_id
    self.segment #default is empty
    self.base_query  #actually i dun think i need this
    '''
    def __init__(self, feature = 'Contact', segment = ''): 
        self.feature = feature #181c5854-792c-41f2-a389-0809e37e611b later on use str.replace
        self.feature_id = ''
        self.segment = segment
        self._select = ''
        self._from = ''
        self._where = ''
        self._order = ''
        self.base_query = ''
        self.second_init_to_call_functions()
    
    def __str__(self): #returns eachpayingseat_sql
        return self._select + self._from + self._where + self._order
    
    #@staticmethod
    def get_add_segment_line_only(self):
        segment = self.segment
        if segment=='': return ''

        to_add = '''
        AND role.name = \'''' + segment + '''\'
        '''
        return to_add
    
    #@staticmethod
    def get_feature_id(self):
        feature = self.feature
        dictionary = {'Contact':'181c5854-792c-41f2-a389-0809e37e611b', 'Export':'8871e629-a8ff-4d01-8d7c-1040eb943d4d'}
        return dictionary[feature]
    
    def second_init_to_call_functions(self):
        segment_line_to_add_to_query = self.get_add_segment_line_only()
        feature_id = self.get_feature_id()

        self._select = '''
            SELECT
                    distinct(ss.id) AS seat_id,
                    u.email,
                    role.name as segment,
                    f.used_count,
                    pr.allowed_count,
                    f.used_count*100.0/pr.allowed_count AS percent_used,
                    cast(to_char(f.created_at, 'YYYY-MM-DD') as date) AS feature_usage_created_month,
                    fg.name AS fg_name,
                    to_char(f.created_at,'yyyy-mm') as feature_usage_created_month_only,
                    CASE 
                        WHEN (f.used_count*100.0/pr.allowed_count) = 0 THEN 0
                        WHEN (f.used_count*100.0/pr.allowed_count) < 20 THEN 20 --20 to 40
                        WHEN (f.used_count*100.0/pr.allowed_count) < 40 THEN 40 --20 to 60
                        WHEN (f.used_count*100.0/pr.allowed_count) < 60 THEN 60 -- 60 to 80
                        --WHEN (f.used_count*100.0/pr.allowed_count) < 80 THEN 80. --80 to 100
                        ELSE 80 END AS bucket --or 100
            '''
        self._from = '''
            FROM visage.user u
                    JOIN customer.subscription_seat ss ON ss.user_id = u.id
                    JOIN visage.feature_usage_period_rule pr ON pr.feature_group_id = ss.feature_group_id
                    LEFT JOIN (SELECT * FROM visage.feature_usage f WHERE feature_id = \'''' + feature_id + '''\') f on f.subscription_seat_id = ss.id
                    JOIN customer.feature_group fg ON fg.id = ss.feature_group_id
                    JOIN visage.user_profile up ON u.id = up.user_id 
                    JOIN visage.role_type role ON up.role_id = role.id
            '''
        self._where = '''
            WHERE
                    NOT u.is_admin AND
                    lower(u.email) not like '%@reonomy.com' AND
                    pr.feature_id = \'''' + feature_id+ '''\' --unique feature
                    AND ss.feature_group_id NOT IN ('f8e00c1f-5a62-43bf-93ea-815f1bce1be7')--exclude national trial
                    ''' + segment_line_to_add_to_query
        self._order = '''
            ORDER BY 2,5 DESC
            '''
        
        self.base_query = self._select + self._from + self._where + self._order


class FinalQuery(object):

    def __init__(self, _type): #type = percentile_table ; consolidate_by_buckets
        self.type = _type
        self.final_query = ""
        self.set_query()

    def __str__(self):
        return self.final_query

    def set_query(self):
        if self.type == 'percentile_table':
            self.final_query = '''
                            SELECT 
                                    to_char(x.feature_usage_created_month,'YYYY-MM') as month,                                    
                                    sum(used_count) AS total_used_count,
                                    sum(allowed_count) AS total_allowed_count,
                                    concat(round(sum(used_count)*100.0/sum(allowed_count),2),'%') AS percent_used,
                                    count(email) AS count_email,
                                    count(distinct(email)) AS dist_email,
                                    concat(round(CAST(percentile_cont(0.25) WITHIN GROUP (ORDER BY percent_used) as numeric),2), '%') as first_quartile,
                                    concat(round(CAST(percentile_cont(0.50) WITHIN GROUP (ORDER BY percent_used) as numeric),2), '%') as second_quartile,
                                    concat(round(CAST(percentile_cont(0.75) WITHIN GROUP (ORDER BY percent_used) as numeric),2), '%') as third_quartile
                            FROM each_paying_seat x
                            WHERE x.fg_name NOT IN ('admin') AND x.feature_usage_created_month is not null
                            GROUP BY month
                            ORDER BY 1 desc
                            ;
                    '''
        
        if self.type == 'consolidate_by_buckets': 
            self.final_query='''
            SELECT feature_usage_created_month_only, bucket, count(*) 
            FROM each_paying_seat
            GROUP BY 1,2
            ORDER BY 1,2
            '''

class SQLQuery(BaseSQLQuery, FinalQuery):

    def __init__(self, feature='Contact', _type = 'consolidate_by_buckets', segment='', startDate='2017-01-01', endDate='2017-06-30'):
        """ returns SQLQuerySettings object"""
        self.feature = feature
        self.type = _type
        self.segment = segment
        self.startDate = startDate
        self.endDate = endDate
        self.full_query = ""

        BaseSQLQuery.__init__(self, feature, segment=segment)
        FinalQuery.__init__(self,_type)

        self.combine_base_and_final()
    
    def __str__(self):
        return self.full_query

    def combine_base_and_final(self):
        self.full_query = '''
        WITH each_paying_seat AS (
        ''' + self.base_query + ')'+ self.final_query
    
    def debug_identify_params(self):
        return self.feature + self.type + self.segment + self.startDate + self.endDate

if __name__=='__main__':
    #testquerty = SQLQuery(_type="percentile_table")
    testquerty = SQLQuery(feature='Export', _type="percentile_table")
    print (testquerty)
