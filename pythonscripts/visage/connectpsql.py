#!/usr/bin/python
import psycopg2
#from pythonscripts.mixpanel.config import config
import pythonscripts.visage.sqlqueries as queries
import pandas as pd
import os

from plotly import tools
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

#to delte and uncomment 3 line
from configparser import ConfigParser
 
def config(filename=os.path.join(os.path.dirname(__file__), r'database.ini'), section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
 
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
 
    return db   
 


#to delete above

#-----------------------------------------Helper Functions to clean up data
def get_df_to_html(table):
    return pd.DataFrame.to_html(table, classes = "w3-table w3-striped w3-white w3-hoverable w3-medium", index=False)

##################### TO RUN QUERY###################

def runQuery(name, segment = ''):
    """Get query"""
    query = queries.getQuery(name, segment=segment)
    
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
        print('am i able to connect?')
        # create a cursor
        cur = conn.cursor()
        # execute a statement
        cur.execute(query)
        # display results
        res = cur.fetchall()
        #col names
        colnames = [desc[0] for desc in cur.description]
        print ("printing colnames just to check that its working: {}".format(colnames))
        #convert results to dataframe
        df = pd.DataFrame.from_records(res, columns = colnames)
        df = df.fillna(0)
        # close the communication with the PostgreSQL
        cur.close()
        return df

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')
 

###################################### PLOT CHARTS I WANT ON THE GO/EVERYTIME I HIT REFRESH
#--------------------- more helper functions
def remove_outliers(arr):
    arr[arr>100.0] = 100.0
    return arr

def remove_null(arr):
    return arr[arr>0.00]

def helper_determine_piechart_buckets(entry):
    entry = float(entry) #originally a decimal type
    if entry == 0.0: return "0%"                             #non-users
    elif (entry > 0.0 and entry < 20.0): return "0%-20%"    #low utility users
    elif (entry >= 20.0 and entry < 40.0): return '20%-40%' #low-med utility users
    elif (entry >= 40.0 and entry < 80.0): return '40%-80%' #med utility users
    elif (entry >= 80.0): return '>=80%'                     #high utility users
    else: return "unsure"

##################### 27th march 2017 ###################
##################### RUN INDIVIDUAL ADHOC QUERIES TO GET SEGMENT ###################
def adhocSegmentQuery_returnHTML(query_name, segment):
    df = runQuery(query_name, segment = segment)
    html = get_df_to_html(df)
    return html


############################ 29th march to get plot. 2 steps, 1) get data 2) plot data
#--------------------- step 1 29th march get stacked bars offline plot
def get_cleaned_data_for_segment(segment='',eachpayingseatBoolean=True):
    featureList = ['contact', 'export']
    res = []
    if eachpayingseatBoolean == False:
        return 
    else:
        #dun need today_mmyyyy = str(datetime.date.today())[:7] #i dun need this
        for feature in featureList:
            raw_df = runQuery(feature+'eachpayingseat', segment)

            raw_df['Bucket'] = raw_df['percent_used'].map(helper_determine_piechart_buckets)
            group_by_buckets_df = raw_df.groupby(['feature_usage_created_month_only','Bucket'])['email'].count().reset_index()
            res.append([feature, segment, group_by_buckets_df])
        return res #returns [[contact data],[export data]]


#--------------------- step 2 29th march get stacked bars offline plot

    #helper for get_stacked_barcharts_utilization_offline_return_filename to get y axis (fill in 0 entries)
def helper_get_y_axis_return_array(group_by_buckets_df, unique_months_arr, buckets): 
    yaxisData = []
    for bucket in buckets:
        ySeriesForBucket = []
        for month in unique_months_arr:
            temp = group_by_buckets_df[group_by_buckets_df['feature_usage_created_month_only']==month]
            temp = temp[temp['Bucket']==bucket]['email']
            if temp.empty:
                temp = 0 
            else:
                temp = temp.iloc[0]
            ySeriesForBucket.append(temp)
        yaxisData.append(ySeriesForBucket)
    return yaxisData

def get_stacked_barcharts_utilization_offline_return_filename(cleaned_data_array):
    group_by_buckets_df = cleaned_data_array[2]
    segment = cleaned_data_array[1]
    feature_name = cleaned_data_array[0]
    unique_months_arr = group_by_buckets_df['feature_usage_created_month_only'].unique()[1:]
    buckets = ["0%", "0%-20%", "20%-40%", "40%-80%", ">=80%"]
    yaxisData = helper_get_y_axis_return_array(group_by_buckets_df, unique_months_arr, buckets) #data for particular bucket only
    data = []
    idx = 0
    chart_title_include_segment = ' for ' + segment if segment != '' else ''
    for bucket in buckets:
        trace = go.Bar(
            x = unique_months_arr,
            y = yaxisData[idx],#group_by_buckets_df[group_by_buckets_df['Bucket']==bucket]['email'],
            name = bucket,
            text = yaxisData[idx], #group_by_buckets_df[group_by_buckets_df['Bucket']==bucket]['email'],
            textposition = 'auto'
        )
        data.append(trace)
        idx += 1
    
    layout = go.Layout(
        barmode='stack',
        title = 'Breakdown of ' + feature_name + ' Utilization Over Time' + chart_title_include_segment,
        xaxis = dict(title='Month'),
        yaxis = dict(title = 'Number of Users')
    )
    fig = go.Figure(data=data, layout=layout)
    
    #plotly.offline.plot(fig, filename='stacked-bar-for' + feature_name + chart_title_include_segment,show_link=)
    #changed this waa confusing aiyo
    filename = plotly.offline.plot(fig, show_link=False, filename='stacked-bar-for' + feature_name + chart_title_include_segment+'.html', auto_open=False,include_plotlyjs=False,output_type='div')
    return filename

# ---------------------- GET LINE CHART ON THE GO 2ND APRIL
def get_linechart(query_name): #contact/export only. generic
    df = runQuery(query_name, segment='')
    layout = go.Layout(
        title= query_name + ' Utilization Over Time')

    percentages_array = df.as_matrix()[:,3]
    month_array = df.as_matrix()[:,0]
    trace = go.Scatter(
        x = month_array,
        y = percentages_array
    )

    fig = go.Figure(data = [trace], layout = layout)
    filename = plotly.offline.plot(fig, show_link=False, filename=query_name + ' utilization line chart.html', auto_open=False,include_plotlyjs=False,output_type='div')
    return filename


if __name__ == '__main__':
    print ('hi')
