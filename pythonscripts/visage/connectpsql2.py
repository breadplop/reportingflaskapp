#!/usr/bin/python
import psycopg2
#from pythonscripts.mixpanel.config import config
#from sqlqueries2 import SQLQuery, BaseSQLQuery, FinalQuery
from pythonscripts.visage.sqlqueries2 import SQLQuery, BaseSQLQuery, FinalQuery
import pandas as pd
import os
#dun need import datetime

from plotly import tools
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

#to delte and uncomment 3 line
from configparser import ConfigParser

 #configurationt to connect to the database
def config2():
    psql_host = os.environ.get('psql_host')
    psql_database = os.environ.get('psql_database')
    psql_user = os.environ.get('psql_user')
    psql_password = os.environ.get('psql_password')
    
    db = {
        'host': psql_host,
        'database': psql_database,
        'user': psql_user,
        'password': psql_password
    }
    return db
    

class ResultsFromDatabase(SQLQuery):
    '''
    Get results from the database either as a dataframe or html (for percentile_table)
    self.results_df
    self.results_html
    self.title_of_data #title of the results
    + params from SQLQuery(contact,_type,segment, startDate, endDate)
    '''
    def __init__(self, feature='Contact', _type = 'consolidate_by_buckets', segment='', startDate='2017-08-01', endDate='2018-03-30'):
        self.results_df = ''
        self.results_html = ''
        super().__init__(feature= feature, _type= _type, segment=segment, startDate=startDate, endDate=endDate)
        self.title_of_data = feature + '_' + _type + '(' + segment + ')'
        self.second_init_to_get_results_as_df_and_html()

    def __str__(self, num_lines = 5):
        preview_results = str(self.results_df.head(num_lines))
        return preview_results

    def second_init_to_get_results_as_df_and_html(self):
        self.runQuery_return_as_df()
        self.get_df_to_html()

    ##################### TO RUN QUERY###################
    def runQuery_return_as_df(self): #returns as df
        query = self.full_query

        """ Connect to the PostgreSQL database server """
        conn = None
        try:
            # read connection parameters
            params = config2()
            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)
            print ('database is connected')
            cur = conn.cursor()            # create a cursor
            cur.execute(query)             # execute a statement
            res = cur.fetchall()            # display results
            colnames = [desc[0] for desc in cur.description]            #col names
            #print ("printing colnames just to check that its working: {}".format(colnames))
            #convert results to dataframe
            df = pd.DataFrame.from_records(res, columns = colnames)
            df = df.fillna(0)
            self.results_df = df
            # close the communication with the PostgreSQL
            cur.close()
            return df

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            if conn is not None:
                conn.close()
                print('Database connection closed.')
    
    #-----------------------------------------Helper Functions to clean up data
    def get_df_to_html(self):
        #table = self.results_df.groupby(['feature_usage_created_month_only','bucket'])['count'].sum().unstack().reset_index()
        table = self.results_df
        html = pd.DataFrame.to_html(table, classes = "w3-table w3-striped w3-white w3-hoverable w3-medium", index=False)
        self.results_html = html
        return html

##############################     PLOTLY     ########################################
class GenericPlotlyChartsComponents(object): #this seems useless
    ''' 
    components plotly charts and diagrams 
    self.trace
    self.data
    self.layout
    self.fig

    self.chart_title
    self.div
    self.filename
    '''
    #stopped here,need to decide what to include
    def __init__(self):
        self.trace = ''
        self.data = [] #stores the traces
        self.layout = ''
        self.fig = ''
        self.chart_title = ''
        self.div = ''
        self.filename = ''

class SubPlotlyBar(GenericPlotlyChartsComponents):
    '''
    self.unique_months_arr #unique_months_arr x
    self.data_for_idv_bucket_only_in_array #data_for_idv_bucket_only y
    self.group_by_buckets_df #from ResultsFromDatabase Object

    & genericplotlychartcomponents
        + trace, data, layout, fig, chart_title, filename (will be empty probably)

    NOT IN SELF
    - resultsfromdatabase
        + results_df, results_html, title_of_data, feature,_type,segment, startDate, endDate
    '''
    def __init__(self, results_from_database_object):
        super().__init__()
        buckets = ["0%", "0%-20%", "20%-40%", "40%-80%", ">=80%"]
        self.buckets = buckets
        self.group_by_buckets_df = results_from_database_object.results_df

        self.unique_months_arr = {}
        self.data_for_idv_bucket_only_in_array = {}
        self.second_init(results_from_database_object)

    def second_init(self,results_from_database_object):
        unique_months_arr_series = self.group_by_buckets_df['feature_usage_created_month_only'].unique()
        self.unique_months_arr = unique_months_arr_series[unique_months_arr_series != 0]
        self.data_for_idv_bucket_only_in_array = self.group_by_buckets_df #wrong
        #self.helper_get_y_axis_return_array(self.group_by_buckets_df, self.unique_months_arr, self.buckets)
        self.set_data()
        self.set_layout(results_from_database_object)
        self.fig = go.Figure(data=self.data, layout=self.layout)
        self.filename = plotly.offline.plot(self.fig, show_link=False, filename='stacked-bar-for' + results_from_database_object.title_of_data +'.html', auto_open=False,include_plotlyjs=False,output_type='div')

    def set_trace(self, idx): #dunnid bucket
        buckets = [0,20,40,60,80]
        bucket = buckets[idx]
        temp_trace = go.Bar(
            x = self.unique_months_arr,
            y = self.helper_for_trace_to_get_y(self.unique_months_arr, self.group_by_buckets_df, bucket),#self.group_by_buckets_df[self.group_by_buckets_df['bucket']==bucket]['count'],#self.data_for_idv_bucket_only_in_array[idx],
            name = bucket,
            #text = self.data_for_idv_bucket_only_in_array[idx], do i need this
            textposition = 'auto'
        )
        #print ('temptrace y ', self.group_by_buckets_df[self.group_by_buckets_df['bucket']==bucket])
        return temp_trace
        #stopped here. charts come out but numbers used are wrong. 
    
    @staticmethod
    def helper_for_trace_to_get_y(unique_months_arr, group_by_buckets_df, bucket):
        y = []
        for month in unique_months_arr:
            #print ('for month {} and bucket {}'.format(month, bucket))
            df_filtered_by_bucket = group_by_buckets_df[group_by_buckets_df['bucket']==bucket]
            df_filtered_by_bucket_and_month = df_filtered_by_bucket[df_filtered_by_bucket['feature_usage_created_month_only']==month]
            #print(df_filtered_by_bucket_and_month)
            count_for_particular_bucket_and_month = list(df_filtered_by_bucket_and_month['count'])[0] if not df_filtered_by_bucket_and_month['count'].empty else 0
            
            if count_for_particular_bucket_and_month >=0:
                y.append(count_for_particular_bucket_and_month)
            else:
                y.append(0)
        return y

    def set_data(self):
        data = []
        for idx in range(0,len(self.buckets)):
            temp_trace = self.set_trace(idx)
            data.append(temp_trace)
        
        self.data = data
    
    def set_layout(self,results_from_database_object):
        self.layout = go.Layout(
            barmode = 'stack',
            title = 'Breakdown of ' + results_from_database_object.feature + ' Utilization Over Time (' + results_from_database_object.segment +')',
            xaxis = dict(title='Month'),
            yaxis = dict(title = 'Number of Users')
        )
    
    @staticmethod
    def helper_get_y_axis_return_array(group_by_buckets_df, unique_months_arr, buckets): 
        yaxisData = []
        for bucket in buckets:
            ySeriesForBucket = []
            for month in unique_months_arr:
                temp = group_by_buckets_df[group_by_buckets_df['feature_usage_created_month_only']==month]
                temp = temp[temp['bucket']==bucket]['email']
                if temp.empty:
                    temp = 0 
                else:
                    temp = temp.iloc[0]
                ySeriesForBucket.append(temp)
            yaxisData.append(ySeriesForBucket)
        return yaxisData

class SubPlotlyLine(GenericPlotlyChartsComponents):
    '''
    self.month_array #x
    self.percentage_array #y

    & genericplotlychartcomponents
        + trace, data, layout, fig, chart_title, filename (will be empty probably)

    NOT IN SELF
    - resultsfromdatabase
        + results_df, results_html, title_of_data, contact,_type,segment, startDate, endDate

    '''
    def __init__(self, results_from_database_object ): #wait shld i inherit resultsfromdatabase? dun think so
        super().__init__()
        df = results_from_database_object.results_df
        self.month_array = df.as_matrix()[:,0] #month array, x
        self.percentage_array = df.as_matrix()[:,3] #percentage array, y
        self.second_init(results_from_database_object)
        
    def second_init(self, results_from_database_object):
        self.layout = go.Layout(
            title = results_from_database_object.title_of_data + ' (Utilization Over Time)'
        )

        self.trace = go.Scatter(
            x = self.month_array,
            y = self.percentage_array
        )

        self.data = [self.trace]

        self.fig = go.Figure(data = [self.trace], layout = self.layout)

        self.filename = plotly.offline.plot(self.fig, show_link=False, filename=results_from_database_object.title_of_data + ' utilization line chart.html', auto_open=False,include_plotlyjs=False,output_type='div')

class ContactExportUtilization(ResultsFromDatabase):
    '''
    self.stacked_bar_utilization_chart
    '''
    def __init__(self,segment=''):
        self.segment = segment
        self.get_data_and_plot()

    def get_data_and_plot(self):
        self.contact_percentile_table = ResultsFromDatabase(feature='Contact', _type = 'percentile_table', segment=self.segment).results_html
        self.export_percentile_table = ResultsFromDatabase(feature='Export', _type = 'percentile_table', segment=self.segment).results_html

        contact_buckets_table = ResultsFromDatabase(feature='Contact', _type = 'consolidate_by_buckets', segment=self.segment)
        export_buckets_table = ResultsFromDatabase(feature='Export', _type = 'consolidate_by_buckets', segment=self.segment)

        self.contact_buckets_plot = SubPlotlyBar(contact_buckets_table).filename
        self.export_buckets_plot = SubPlotlyBar(export_buckets_table).filename



if __name__ == '__main__':
    '''
    print ('hi')
    buckets_table = ResultsFromDatabase(feature='Export', _type = 'consolidate_by_buckets', segment='Sales Broker' )
    print(buckets_table.full_query)
    print(buckets_table.results_df)
    df = buckets_table.results_df
    print(df.columns)
    new_df = df.groupby(['feature_usage_created_month_only','bucket'])['count'].sum().unstack()#reset_index()
    #new_Df = pd.pivot_table(df,values=df['count'], index=[0,20,40,60,80], columns=df['feature_usage_created_month_only'], aggfunc='sum')
    print('new df is ', new_df)
    
    chart_b = SubPlotlyBar(buckets_table)
    print(chart_b.filename)
    '''
    html_table_export = ResultsFromDatabase(feature='Export', _type = 'consolidate_by_buckets', segment='Sales Broker')
    htmlfilename_export = SubPlotlyBar(html_table_export).filename
    #print(htmlfilename_export)
    #buckets_table2 = ResultsFromDatabase(feature='Export', _type = 'consolidate_by_buckets', segment='Sales Broker' )
    #print(buckets_table2.__str__())
    #chart_b2 = SubPlotlyBar(buckets_table2)
    #print(chart_b2.filename)

