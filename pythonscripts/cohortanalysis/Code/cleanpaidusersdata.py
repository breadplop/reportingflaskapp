'''
====================================
Clean up raw data downloaded from National Data Dashboard
====================================
Tasks: create functions to clean up the data and keep information that we want
to create pivot charts

How it works
    - Download the 3 raw files from National Data Dashboard Google Spread Sheet (Paid Users | Churned/Downgrades | Upgrades)
    - Run python file


Steps required to clean:
1) load file
2) clean file 
3) pivot charts
'''
import pandas as pd
import datetime as dt
import numpy as np

import seaborn as sns
from matplotlib import pyplot as plt

class GlobalVariables(object):
    def __init__(self):
        self.wanted_paid_columns = ['Trial Start Date', 'Date of Conversion', 'Acquisition Source', 'utm_campaign', 'MRR', 'conversion_month','conversion_weeknum']
        self.wanted_upgrade_columns = ['Original Sign Up Date','Upgrade Date','Previous Tier','New Tier','Upgrade Amount','Upgrade Year','Upgrade Month','Upgrade WeekNum','Cohort Month','Cohort Year','signup and upgrade in the same month','Outbound/Inbound']
        self.wanted_lost_columns = ['Outcome', 'Previous Tier (MRR)', 'New Tier (MRR)', 'Churn Date (Contract Termination)','Date of Downgrade', 'Inbound/Outbound','Customer Segment', 'Cohort Date']

        self.params = {
            'paid': 0,
            'upgrade': 0,
            'lost': 0,
            'churn': 0,
        }

        self.params_for_plotting_function = {
            'mrr_cohort_default_5' : {
                #'table': _list[0],
                'percentFlag': False,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '11',
                'customTitle' : 'Cohorts: MRR Retention',
                'numonboardingmonths': False
            },
            'mrr_cohort_default_percent_6' : {
                #'table': _list[1],
                'percentFlag': True,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '11percent',
                'customTitle' : 'Cohorts: MRR Retention (%)',
                'numonboardingmonths': False
            },
            'user_cohort_default_1' : {
                #'table': _list[2],
                'percentFlag': False,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '12',
                'customTitle' : 'Cohorts: User Retention',
                'numonboardingmonths': False
            },
            'user_cohort_default_percent_2' : {
                #'table': _list[3],
                'percentFlag': True,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '12percent',
                'customTitle' : 'Cohorts: User Retention (%)',
                'numonboardingmonths': False
            },
            'mrr_cohort_num_onboarding_months_7' : {
                #'table': _list[4],
                'percentFlag': False,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '21',
                'customTitle' : 'Cohorts: MRR Retention by Number of Onboarding Months',
                'numonboardingmonths': True
            },
            'mrr_cohort_num_onboarding_months_percent_8' : {
                #'table': _list[5],
                'percentFlag': True,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '21percent',
                'customTitle' : 'Cohorts: MRR Retention by Number of Onboarding Months (%)',
                'numonboardingmonths': True
            },
            'user_cohort_num_onboarding_months_3' : {
                #'table': _list[6],
                'percentFlag': False,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '22',
                'customTitle' : 'Cohorts: User Retention by Number of Onboarding Months',
                'numonboardingmonths': True
            },
            'user_cohort_num_onboarding_months_percent_4' : {
                #'table': _list[7],
                'percentFlag': True,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '22percent',
                'customTitle' : 'Cohorts: User Retention by Number of Onboarding Months (%)',
                'numonboardingmonths': True
            }  
        }

class LoadCleanRawData(GlobalVariables):
    def __init__(self, location, name):
        self.location = location
        self.name= name
        self.raw_df = pd.read_csv(location)
        self.cleanData()
    
    def cleanData(self):
        if self.name == 'paid':
            df = self.helper_replace_secondRowasHeader(self.raw_df)
            df = df.replace(np.nan, '-')
            print(df.head())
            df = pd.DataFrame(df[['Trial Start Date ', 'Date of Conversion ','MRR']])
            df['subscription_start_date'] = df['Date of Conversion '].apply(lambda x: dt.datetime.strptime(x,'%Y-%m-%d') if x != '-' else x)
            df['subscription_start_month'] = df['subscription_start_date'].apply(lambda x: x.strftime('%Y-%m') if x != '-' else x) 
            
            # -- reset MRR into float object so that we can calculate it
            df = df.replace({'\$':'',',':''}, regex=True)
            df['Cleaned MRR'] = df['MRR'].replace('-',0)
            df['Cleaned MRR'] = df['Cleaned MRR'].astype(float)
        
        if self.name == 'upgrade':
            df = pd.DataFrame(self.raw_df[['Original Sign Up Date', 'Previous Tier', 'New Tier','Upgrade Date','signup and upgrade in the same month','Outbound/Inbound']])
            df['Upgrade Amt'] = df['New Tier'] - df['Previous Tier']
            df['subscription_start_date'] = df['Original Sign Up Date'].apply(lambda x: dt.datetime.strptime(x,'%Y-%m-%d'))
            df['subscription_start_month'] = df['subscription_start_date'].apply(lambda x: x.strftime('%Y-%m'))
            df['Upgrade Date']= self.helper_convert_to_date(df['Upgrade Date'])
            df['upgrade_month'] = self.helper_get_yyyymm(df['Upgrade Date']) 
        
        if self.name == 'lost':
            df = self.helper_replace_secondRowasHeader(self.raw_df)
            df = df.replace({np.nan: '-', '\$':''}, regex=True)
            df = pd.DataFrame(df[['Outcome', 'Previous Tier (MRR)', 'New Tier (MRR)', 'Churn Date (Contract Termination)','Date of Downgrade', 'Inbound/Outbound','Customer Segment', 'Cohort Date']])
            df['Cleaned Old Tier'] = df['Previous Tier (MRR)'].replace({'-':0, 'TBD':0})
            df['Cleaned New Tier'] = df['New Tier (MRR)'].replace({'-':0, 'TBD':0})
            df['Lost Amt'] = df['Cleaned Old Tier'].astype(float) - df['Cleaned New Tier'].astype(float)
            df['subscription_start_date'] = df['Cohort Date'].apply(lambda x: '-' if (x == '-' or len(x) != 10) else dt.datetime.strptime(x,'%Y-%m-%d'))
            df['subscription_start_month'] = df['subscription_start_date'].apply(lambda x: '-' if x=='-' else x.strftime('%Y-%m'))
        
            df['Cleaned Churn Date'] = self.helper_convert_to_date(df['Churn Date (Contract Termination)'])
            df['Churn Month'] = self.helper_get_yyyymm(df['Cleaned Churn Date'])
            
            df = self.helper_setLostMRRDate(df)
        #print(df.head())
        self.clean_data = df

    @staticmethod
    def helper_convert_to_date(data_col, noreplace = False):
        data_col = data_col.replace({np.nan: '-', '\$':'', 'TBD':'-'}, regex=True) if noreplace == False else data_col
        data_col = data_col.apply(lambda x: '-' if x=='-' or x == '' or x == 'TBD' or len(x) != 10 else dt.datetime.strptime(x,'%Y-%m-%d'))
        return data_col
    @staticmethod
    def helper_get_yyyymm(data_col):
        data_col = data_col.apply(lambda x: '-' if x=='-' or x == '' else x.strftime('%Y-%m'))
        return data_col
    @staticmethod
    def helper_replace_secondRowasHeader(data_file):
        df = data_file
        header = df.iloc[0]
        df = df[1:]
        df.columns = header
        df.reset_index
        return df

    def helper_setLostMRRDate(self,df):
        df['Churn Date (Contract Termination)'] = df['Churn Date (Contract Termination)'].replace('-','_')
        df['Date of Downgrade'] = df['Date of Downgrade'].replace('-','_')
        df['lost_mrr_date'] = df['Churn Date (Contract Termination)'].map(str) + df['Date of Downgrade']
        df['lost_mrr_date'] = df['lost_mrr_date'].replace('_','', regex=True) 
        df['lost_mrr_date'] = self.helper_convert_to_date(df['lost_mrr_date'], noreplace=True)
        df['lost_mrr_month'] = self.helper_get_yyyymm(df['lost_mrr_date'])
        return df

class CreatePivotData(LoadCleanRawData):
    def __init__(self, location,name):
        '''
        to create pivot tables (pivot_table [aggregate: sum] and pivot_table_count [agrregate: count]) 
        pivot_table - for tabulation of mrr cohorts
        pivot_table_count - for tabulation of user cohorts (relevant only for paid and churned users)
        '''
        super().__init__(location, name)
        self.createPivotCharts() #creates self.pivot_table

    def createPivotCharts(self):
        _type = self.name 
        _cleaned_data = self.clean_data
        if _type == 'paid':
            col_to_calc = 'Cleaned MRR'        
            action_month = 'subscription_start_month2'
            _cleaned_data['subscription_start_month2'] = _cleaned_data['subscription_start_month'] 

            self.pivot_table = self.helper_clean_pivot_charts(_cleaned_data, 'sum', action_month, col_to_calc)
            self.pivot_table_count = self.helper_clean_pivot_charts(_cleaned_data, 'count', action_month, col_to_calc)
            
        if _type == 'lost':
            col_to_calc = 'Lost Amt'
            action_month = 'lost_mrr_month'
            _cleaned_data = _cleaned_data 
            _cleaned_data_churned_only = _cleaned_data[_cleaned_data['Outcome']== 'Churned'] #remove downgrade info
            self.pivot_table = self.helper_clean_pivot_charts(_cleaned_data, 'sum', action_month, col_to_calc)
            self.pivot_table_count = self.helper_clean_pivot_charts(_cleaned_data_churned_only, 'count', action_month, col_to_calc)

        if _type == 'upgrade':
            col_to_calc = 'Upgrade Amt'
            action_month = 'upgrade_month'

            self.pivot_table = self.helper_clean_pivot_charts(_cleaned_data, 'sum', action_month, col_to_calc)
        
        
    @staticmethod
    def helper_clean_pivot_charts(_cleaned_data, aggregate, action_month, col_to_calc): #drop NA, remove '-'
        col_to_groupby = ['subscription_start_month', action_month] #if _type !='Paid' else ['subscription_start_month']
        pivot_table = _cleaned_data.groupby(col_to_groupby)[col_to_calc].agg(aggregate).reset_index() 
        pivot_table = pivot_table.pivot_table(columns = [action_month], index = 'subscription_start_month', values = col_to_calc)

        pivot_table = pd.DataFrame(pivot_table)
        pivot_table = pivot_table.replace(np.nan,'')
        pivot_table = pivot_table.drop('-', axis=0) if '-' in pivot_table.index else pivot_table
        pivot_table = pivot_table.drop('-', axis=1) if '-' in pivot_table.columns else pivot_table
        pivot_table = pivot_table.replace('',0.0) if aggregate == 'count' else pivot_table
        return pivot_table

class CreateDataDictionaryForAllPivotData(object):
    def __init__(self, paid_CreatePivotData_object, lost_CreatePivotData_object, upgrade_CreatePivotData_object):
        self.paid_obj = paid_CreatePivotData_object
        self.lost_obj = lost_CreatePivotData_object
        self.upgrade_obj = upgrade_CreatePivotData_object

class CreateCohortTables(CreateDataDictionaryForAllPivotData, GlobalVariables):
    '''
    to create 10 charts from 3 files:
    1. default user cohort analysis  | 2percentage   <related>      3. user cohort analysis by number of onboarding months   | 4percentage
    6. default mrr cohort analysis   | 6percentage   <related>      7. mrr cohort analysis by number of onboarding months    | 8percentage
    9. retention curves by user                        10. retention curves by mrr
    
    Attributes:
    self.user_cohort_default_1
    self.user_cohort_default_percent_2
    self.user_cohort_num_onboarding_months_3
    self.user_cohort_num_onboarding_months_percent_4
    self.mrr_cohort_default_5
    self.mrr_cohort_default_percent_6
    self.mrr_cohort_num_onboarding_months_7
    self.mrr_cohort_num_onboarding_months_percent_8
    '''
    def __init__(self, paid_CreatePivotData_object, lost_CreatePivotData_object, upgrade_CreatePivotData_object):
        CreateDataDictionaryForAllPivotData.__init__(self,paid_CreatePivotData_object, lost_CreatePivotData_object, upgrade_CreatePivotData_object)
        GlobalVariables.__init__(self)
        self.create_user_cohort_charts()
        self.create_mrr_cohort_charts()

    def create_user_cohort_charts(self):
        '''
        to create chart 1/2/3/4 
        required data: churned and paid only
        '''
        pivot_user_churned = self.lost_obj.pivot_table_count
        pivot_user_paid = self.paid_obj.pivot_table_count
        full_list_action_col, full_list_cohort_grp_index = self.getFullListColAndIndex(pivot_user_churned,pivot_user_paid) #action_month,cohort_month
        table_churn_user = self.addMissingColAndIndexAndReSort(self,pivot_user_churned, full_list_action_col, full_list_cohort_grp_index)
        table_new_user = self.addMissingColAndIndexAndReSort(self,pivot_user_paid, full_list_action_col, full_list_cohort_grp_index)

        cum_table_churn_user = table_churn_user.cumsum(axis=1)
        cum_table_new_user = table_new_user.cumsum(axis = 1)

        df_of_count_per_month = table_new_user.max().reset_index().set_index('subscription_start_month2')
        res = cum_table_new_user - cum_table_churn_user
        res = res.replace(0.0, np.nan).dropna(thresh = 1)
        res = self.remove_extra_months(res)

        self.user_cohort_default_1 = res
        self.user_cohort_default_percent_2 = self.create_percentage_chart(res, df_of_count_per_month)
        self.user_cohort_num_onboarding_months_3 = self.reset_data_by_onboarding_month(self.user_cohort_default_1)
        self.user_cohort_num_onboarding_months_percent_4 = self.reset_data_by_onboarding_month(self.user_cohort_default_percent_2)

        self.params_for_plotting_function['user_cohort_default_1']['table'] = self.user_cohort_default_1
        self.params_for_plotting_function['user_cohort_default_percent_2']['table'] = self.user_cohort_default_percent_2
        self.params_for_plotting_function['user_cohort_num_onboarding_months_3']['table'] = self.user_cohort_num_onboarding_months_3
        self.params_for_plotting_function['user_cohort_num_onboarding_months_percent_4']['table'] = self.user_cohort_num_onboarding_months_percent_4
    def create_mrr_cohort_charts(self): 
        '''
            to create chart 5/6/7/8
            required data: lost, paid, upgrade
        '''
        pivot_mrr_churned_downgrades, pivot_mrr_paid, pivot_mrr_upgrades = self.lost_obj.pivot_table, self.paid_obj.pivot_table, self.upgrade_obj.pivot_table
        
        full_list_action_col, full_list_cohort_grp_index = self.getFullListColAndIndex(pivot_mrr_churned_downgrades,pivot_mrr_paid) #action_month,cohort_month
        table_mrr_churned_downgrades = self.addMissingColAndIndexAndReSort(self,pivot_mrr_churned_downgrades, full_list_action_col, full_list_cohort_grp_index)
        table_mrr_paid = self.addMissingColAndIndexAndReSort(self,pivot_mrr_paid, full_list_action_col, full_list_cohort_grp_index)
        table_mrr_upgrades = self.addMissingColAndIndexAndReSort(self,pivot_mrr_upgrades, full_list_action_col, full_list_cohort_grp_index)

        cum_table_mrr_churn_downgrades = table_mrr_churned_downgrades.cumsum(axis=1)
        cum_table_mrr_paid = table_mrr_paid.cumsum(axis=1)
        cum_table_mrr_upgrades = table_mrr_upgrades.cumsum(axis=1)

        df_of_mrr_per_month = table_mrr_paid.max().reset_index().set_index('subscription_start_month2')
        res = cum_table_mrr_paid + cum_table_mrr_upgrades - cum_table_mrr_churn_downgrades
        res = res.replace(0.0, np.nan).dropna(thresh = 1)
        res = self.remove_extra_months(res)

        self.mrr_cohort_default_5 = res
        self.mrr_cohort_default_percent_6 = self.create_percentage_chart(res, df_of_mrr_per_month)
        self.mrr_cohort_num_onboarding_months_7 = self.reset_data_by_onboarding_month(self.mrr_cohort_default_5)
        self.mrr_cohort_num_onboarding_months_percent_8 = self.reset_data_by_onboarding_month(self.mrr_cohort_default_percent_6)

        self.params_for_plotting_function['mrr_cohort_default_5']['table'] = self.mrr_cohort_default_5
        self.params_for_plotting_function['mrr_cohort_default_percent_6']['table'] = self.mrr_cohort_default_percent_6
        self.params_for_plotting_function['mrr_cohort_num_onboarding_months_7']['table'] = self.mrr_cohort_num_onboarding_months_7
        self.params_for_plotting_function['mrr_cohort_num_onboarding_months_percent_8']['table'] = self.mrr_cohort_num_onboarding_months_percent_8

    @staticmethod
    def addMissingColAndIndexAndReSort(self, df, full_list_col, full_list_index):
        for c in full_list_col:
            if c not in list(df.columns):
                df[c] = 0.0
        
        for i in full_list_index:
            empty_row = [0.0]*len(full_list_col)
            if i not in list(df.index):
                df.loc[i] = empty_row
        
        df = df.reindex_axis(sorted(df.columns), axis=1)
        df = df.reindex_axis(sorted(df.columns), axis=0)
        df = df.fillna(0.0)
        df = df.replace('',0.0)
        return df

    @staticmethod
    def getFullListColAndIndex(df1,df2):
        hi = pd.concat([df1,df2], join='outer')
        all_col = hi.columns
        all_index = hi.index
        full_list_col = list(all_col)
        full_list_index = list(all_index)
        return full_list_col, full_list_index
    
    @staticmethod
    def remove_extra_months(table):
        months_to_remove = []
        current_month = dt.datetime.now().strftime('%Y-%m')
        for col in table.columns.values:
            if col>current_month:
                months_to_remove.append(col)

        table = table.drop(months_to_remove, axis=1)
        
        return table

    @staticmethod
    def create_percentage_chart(res, df_of_count_per_month):
        num_col = len(list(res.columns))
        percent_res = res.copy()
        for idx in percent_res.index:
            percent_res.loc[idx] = [df_of_count_per_month.loc[idx][0]] * num_col
            
        res = (res/percent_res).round(decimals=2)
        res = res.replace(0.0, np.nan).dropna(thresh = 1)
        return res
    @staticmethod
    def reset_data_by_onboarding_month(res):
        num_col = len(res.columns)
        row = list(res.index)
        res_by_onboarding_month = pd.DataFrame(columns=range(0,num_col), index=row)

        counter = 0
        for idx in res_by_onboarding_month.index: 
            value_list = list(res.loc[idx].shift(-counter))
            res_by_onboarding_month.loc[idx] = value_list
            counter += 1

        res_by_onboarding_month = res_by_onboarding_month.replace(0,np.nan).dropna(thresh=1)
        return res_by_onboarding_month

class CreateCohortPlots(CreateCohortTables):
    def __init__(self,paid_CreatePivotData_object, lost_CreatePivotData_object, upgrade_CreatePivotData_object):
        CreateCohortTables.__init__(self,paid_CreatePivotData_object, lost_CreatePivotData_object, upgrade_CreatePivotData_object)
        self.plotAllCharts()

    @staticmethod
    #def plotChart(user_retention_table, percentFlag = False, mrrFlag = False, showFig = False, customFigName = '', customTitle = 'Cohorts: User Retention', numonboardingmonths = False):
    def plotChart(dict_of_info, showFig=False):
        user_retention_table, percentFlag, mrrFlag, showFig, customFigName, customTitle, numonboardingmonths = dict_of_info['table'], dict_of_info['percentFlag'], dict_of_info['mrrFlag'], \
               showFig, dict_of_info['customFigName'], dict_of_info['customTitle'], dict_of_info['numonboardingmonths']

        sns.set_context("poster", font_scale=55)
        format_values_as = '.1%' if percentFlag == True else '.0f'
        sns.set(style='white')
        plt.figure(figsize=(22, 10))
        plt.title(customTitle)
        ax = sns.heatmap(user_retention_table, mask=user_retention_table.isnull(), annot=True, fmt= format_values_as)
        if numonboardingmonths == True:
            plt.xlabel('Number of Onboarding Months')
        else:
            plt.xlabel('Cohort Month')
        plt.ylabel('Cohort Month')
        if mrrFlag == True and percentFlag == False:
            for t in ax.texts: t.set_text("$" + t.get_text())
        if customFigName != '':
            plt.savefig('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/static/img/cohortanalysis2/' + customFigName)
        if showFig == True:
            plt.show()

    @staticmethod
    def plotRetentionCurves(dataNameStr, create_plot_params_dict, showFig = False):
        data = create_plot_params_dict[dataNameStr]['table']
        title = create_plot_params_dict[dataNameStr]['customTitle']

        #add average to data
        data.loc['Average'] = data.mean()
        data = data.dropna(thresh=2, axis=1)
        plt.close('all')
        plt.figure(figsize=(10,8))
        sns.set(style='ticks')
        ax = plt.subplot()
        for idx in data.index.values:
            rowData = data.loc[idx].reset_index()
            if idx != 'Average' and idx > '2018-01': #CHANGE HERE TO DRAW MORE LINES
                #LINES THAT YOU WANT TO SKIP GOES HERE
                pass
            else:

                sns.regplot('index', idx, data=rowData, label = idx, ci = None, truncate=True, order=2)

        ax.legend()  
        plt.title(title)
        plt.savefig('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/static/img/cohortanalysis2/retention_curves_' + dataNameStr)
        if showFig == True:
            plt.show()

    def plotAllCharts(self):
        #--------- plot 1-8
        for entry in self.params_for_plotting_function.items():
            dict_of_info = entry[1]
            self.plotChart(dict_of_info, False)
        #-------- plot 9/10 (retention curves)
        self.plotRetentionCurves('mrr_cohort_num_onboarding_months_percent_8',self.params_for_plotting_function)
        self.plotRetentionCurves('user_cohort_num_onboarding_months_percent_4',self.params_for_plotting_function)

def run_main():
    #upgrade, lost, paid
    main = {
        'upgrade': {
            'location':'/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/pythonscripts/cohortanalysis/RawData/National Data Dashboard - Upgrade List.csv'
        },
        'lost': {
            'location':'/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/pythonscripts/cohortanalysis/RawData/National Data Dashboard - Churned%2FDowngraded Users.csv'
        },
        'paid': {
            'location': '/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/pythonscripts/cohortanalysis/RawData/National Data Dashboard - Paid Users .csv'
        }

    }

    list_objects = []
    for name, items in main.items():
        location = items['location']
        list_objects.append(CreatePivotData(location, name))

    upgrade_obj, lost_obj, paid_obj= list_objects[0], list_objects[1], list_objects[2]
    data_dict = CreateCohortPlots(paid_obj, lost_obj, upgrade_obj)
    #print(data_dict)
    #print("contains: ", data_dict.paid_obj.clean_data.head())
    #print(" cohort table is ", data_dict.user_cohort_num_onboarding_months_percent_4)

if __name__ == '__main__':
    run_main()

        
#NOTES
'''
INCREASE/DECREASE LINES DRAWN IN RETENTION CURVES: look at plotRetentionCurves() function

'''