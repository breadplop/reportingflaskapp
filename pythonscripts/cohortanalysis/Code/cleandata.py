'''
can delete
====================================
Clean up raw data downloaded from National Data Dashboard
====================================
Tasks: create functions to clean up the data and keep information that we want
to create pivot charts

How it works
    - Download the 3 raw files from National Data Dashboard Google Spread Sheet (Paid Users | Churned/Downgrades | Upgrades)
    - Run python file
'''
import pandas as pd
import datetime as dt
import numpy as np

import seaborn as sns
from matplotlib import pyplot as plt

#####################  Global Variables #####################
current_month = dt.datetime.now().strftime('%Y-%m')


#------------ helpers
def createMasterDict(_list):
    create_plot_params_dict = {
            'basic_mrr_cohort_table11' : {
                'table': _list[0],
                'percentFlag': False,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '11',
                'customTitle' : 'Cohorts: MRR Retention',
                'numonboardingmonths': False
            },
            'basic_mrr_cohort_table_percent11' : {
                'table': _list[1],
                'percentFlag': True,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '11percent',
                'customTitle' : 'Cohorts: MRR Retention (%)',
                'numonboardingmonths': False
            },
            'basic_user_cohort_table12' : {
                'table': _list[2],
                'percentFlag': False,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '12',
                'customTitle' : 'Cohorts: User Retention',
                'numonboardingmonths': False
            },
            'basic_user_cohort_table_percent12' : {
                'table': _list[3],
                'percentFlag': True,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '12percent',
                'customTitle' : 'Cohorts: User Retention (%)',
                'numonboardingmonths': False
            },
            'by_onboarding_month_mrr_cohort_table21' : {
                'table': _list[4],
                'percentFlag': False,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '21',
                'customTitle' : 'Cohorts: MRR Retention by Number of Onboarding Months',
                'numonboardingmonths': True
            },
            'by_onboarding_month_mrr_cohort_table_percent21' : {
                'table': _list[5],
                'percentFlag': True,
                'mrrFlag' : True,
                'showFig' : False,
                'customFigName' : '21percent',
                'customTitle' : 'Cohorts: MRR Retention by Number of Onboarding Months (%)',
                'numonboardingmonths': True
            },
            'by_onboarding_month_user_cohort_table22' : {
                'table': _list[6],
                'percentFlag': False,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '22',
                'customTitle' : 'Cohorts: User Retention by Number of Onboarding Months',
                'numonboardingmonths': True
            },
            'by_onboarding_month_user_cohort_table_percent22' : {
                'table': _list[7],
                'percentFlag': True,
                'mrrFlag' : False,
                'showFig' : False,
                'customFigName' : '22percent',
                'customTitle' : 'Cohorts: User Retention by Number of Onboarding Months (%)',
                'numonboardingmonths': True
            }  
    }
    return create_plot_params_dict


#####################  Get Raw Data Files into DataFrames #####################
raw_churned_downgrades = pd.read_csv('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/pythonscripts/cohortanalysis/RawData/National Data Dashboard - Churned%2FDowngraded Users.csv')
raw_paid_users = pd.read_csv('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/pythonscripts/cohortanalysis/RawData/National Data Dashboard - Paid Users .csv')
raw_upgrades = pd.read_csv('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/pythonscripts/cohortanalysis/RawData/National Data Dashboard - Upgrade List.csv')

#####################  Clean up into usable pivot charts #####################

def helper_replace_secondRowasHeader(data_file):
    df = data_file
    header = df.iloc[0]
    df = df[1:]
    df.columns = header
    df.reset_index
    return df

def helper_convert_to_date(data_col, noreplace = False):
    data_col = data_col.replace({np.nan: '-', '\$':'', 'TBD':'-'}, regex=True) if noreplace == False else data_col
    data_col = data_col.apply(lambda x: '-' if x=='-' or x == '' or x == 'TBD' or len(x) != 10 else dt.datetime.strptime(x,'%Y-%m-%d'))
    return data_col

def helper_get_yyyymm(data_col):
    data_col = data_col.apply(lambda x: '-' if x=='-' or x == '' else x.strftime('%Y-%m'))
    return data_col

def helper_clean_pivot_charts(pivot_table): #drop NA, remove '-'
    pivot_table = pd.DataFrame(pivot_table)
    pivot_table = pivot_table.replace(np.nan,'')
    pivot_table = pivot_table.drop('-', axis=0) if '-' in pivot_table.index else pivot_table
    pivot_table = pivot_table.drop('-', axis=1) if '-' in pivot_table.columns else pivot_table
    return pivot_table

def setLostMRRDate(df):
    df['Churn Date (Contract Termination)'] = df['Churn Date (Contract Termination)'].replace('-','_')
    df['Date of Downgrade'] = df['Date of Downgrade'].replace('-','_')
    df['lost_mrr_date'] = df['Churn Date (Contract Termination)'].map(str) + df['Date of Downgrade']
    df['lost_mrr_date'] = df['lost_mrr_date'].replace('_','', regex=True) 
    df['lost_mrr_date'] = helper_convert_to_date(df['lost_mrr_date'], noreplace=True)
    df['lost_mrr_month'] = helper_get_yyyymm(df['lost_mrr_date'])
    return df

#-----------------------------------------------------------------------

def cleanRawData(_type, _data_file): #to further modularise this function
    if _type == 'Paid':
        _data_file = helper_replace_secondRowasHeader(_data_file)
        df = _data_file.replace(np.nan, '-')
        df = pd.DataFrame(df[['Trial Start Date ', 'Date of Conversion ','MRR']])
        df['subscription_start_date'] = df['Date of Conversion '].apply(lambda x: dt.datetime.strptime(x,'%Y-%m-%d') if x != '-' else x)
        df['subscription_start_month'] = df['subscription_start_date'].apply(lambda x: x.strftime('%Y-%m') if x != '-' else x) 
        
        # -- reset MRR into float object so that we can calculate it
        df = df.replace({'\$':'',',':''}, regex=True)
        df['Cleaned MRR'] = df['MRR'].replace('-',0)
        df['Cleaned MRR'] = df['Cleaned MRR'].astype(float)
        
    if _type == 'Lost':
        _data_file = helper_replace_secondRowasHeader(_data_file)
        df = _data_file.replace({np.nan: '-', '\$':''}, regex=True)
        df = pd.DataFrame(df[['Outcome', 'Previous Tier (MRR)', 'New Tier (MRR)', 'Churn Date (Contract Termination)','Date of Downgrade', 'Inbound/Outbound','Customer Segment', 'Cohort Date']])
        df['Cleaned Old Tier'] = df['Previous Tier (MRR)'].replace({'-':0, 'TBD':0})
        df['Cleaned New Tier'] = df['New Tier (MRR)'].replace({'-':0, 'TBD':0})
        df['Lost Amt'] = df['Cleaned Old Tier'].astype(float) - df['Cleaned New Tier'].astype(float)
        df['subscription_start_date'] = df['Cohort Date'].apply(lambda x: '-' if (x == '-' or len(x) != 10) else dt.datetime.strptime(x,'%Y-%m-%d'))
        df['subscription_start_month'] = df['subscription_start_date'].apply(lambda x: '-' if x=='-' else x.strftime('%Y-%m'))
        
        df['Cleaned Churn Date'] = helper_convert_to_date(df['Churn Date (Contract Termination)'])
        df['Churn Month'] = helper_get_yyyymm(df['Cleaned Churn Date'])
        
        df = setLostMRRDate(df)
        
    if _type == 'Upsell':
        df = pd.DataFrame(_data_file[['Original Sign Up Date', 'Previous Tier', 'New Tier','Upgrade Date','signup and upgrade in the same month','Outbound/Inbound']])
        df['Upgrade Amt'] = df['New Tier'] - df['Previous Tier']
        df['subscription_start_date'] = df['Original Sign Up Date'].apply(lambda x: dt.datetime.strptime(x,'%Y-%m-%d'))
        df['subscription_start_month'] = df['subscription_start_date'].apply(lambda x: x.strftime('%Y-%m'))
        df['Upgrade Date']= helper_convert_to_date(df['Upgrade Date'])
        df['upgrade_month'] = helper_get_yyyymm(df['Upgrade Date'])
    
    return df

#-----------------------------------------------------------------------

def createPivotCharts(_type, aggregate, _cleaned_data):
    if _type == 'Paid':
        col_to_calc = 'Cleaned MRR'        
        action_month = 'subscription_start_month2'
        _cleaned_data['subscription_start_month2'] = _cleaned_data['subscription_start_month']
        '''pivot_table = _cleaned_data.groupby('subscription_start_month')['Cleaned MRR'].agg(aggregate).reset_index()
        pivot_table = pd.DataFrame(pivot_table['Cleaned MRR'].values, \
                            columns = ['Total MRR/Users'], \
                            index = pivot_table.subscription_start_month.values)
        pivot_table = helper_clean_pivot_charts(pivot_table)
        return pivot_table'''
    
    if _type == 'Lost':
        col_to_calc = 'Lost Amt'
        action_month = 'lost_mrr_month'
        _cleaned_data = _cleaned_data if aggregate == 'sum' else _cleaned_data[_cleaned_data['Outcome']== 'Churned'] #remove downgrade info
    

    if _type == 'Upsell':
        col_to_calc = 'Upgrade Amt'
        action_month = 'upgrade_month'
    
    col_to_groupby = ['subscription_start_month', action_month] #if _type !='Paid' else ['subscription_start_month']
    pivot_table = _cleaned_data.groupby(col_to_groupby)[col_to_calc].agg(aggregate).reset_index()
    
    pivot_table = pivot_table.pivot_table(columns = [action_month], index = 'subscription_start_month', values = col_to_calc)
    pivot_table = helper_clean_pivot_charts(pivot_table)
        
    return pivot_table

#####################  Helper functions that helps combine charts together to create cohort charts #####################
#-----------------------------------------------------------------------

def helperReSortColandIdx(df):
    df = df.reindex_axis(sorted(df.columns), axis=1)
    df = df.reindex_axis(sorted(df.columns), axis=0)
    return df

#-----------------------------------------------------------------------

def getFullListColAndIndex(df1,df2):
    hi = pd.concat([df1,df2], join='outer')
    all_col = hi.columns
    all_index = hi.index
    full_list_col = list(all_col)
    full_list_index = list(all_index)
    return full_list_col, full_list_index

#-----------------------------------------------------------------------

def addMissingColAndIndexAndReSort(df, full_list_col, full_list_index):
    for c in full_list_col:
        if c not in list(df.columns):
            df[c] = 0.0
    
    for i in full_list_index:
        empty_row = [0.0]*len(full_list_col)
        if i not in list(df.index):
            df.loc[i] = empty_row
    
    df = helperReSortColandIdx(df)
    df = df.fillna(0.0)
    df = df.replace('',0.0)
    return df

#-----------------------------------------------------------------------


#####################  Finally, create the cohort charts! (in dataframe/table format) #####################
'''
There will be a few types of cohort charts:
1. Basic Cohort Charts 
    1.1 by MRR  (completed)
    1.2 by User (completed, need to check accuracy and some simple clean up)
2. Cohort Charts based on number of onboarding months
    2.1 by MRR
    2.2 by User

Completed:

'''

def create_user_CohortAnalysisTable(pivot_user_churned, pivot_user_paid):  #plot12
    '''
    to create cohort analysis table by user count
    '''    
    full_list_action_col, full_list_cohort_grp_index = getFullListColAndIndex(pivot_user_churned,pivot_user_paid) #action_month,cohort_month

    table_churn_user = addMissingColAndIndexAndReSort(pivot_user_churned, full_list_action_col, full_list_cohort_grp_index)
    table_new_user = addMissingColAndIndexAndReSort(pivot_user_paid, full_list_action_col, full_list_cohort_grp_index)

    cum_table_churn_user = table_churn_user.cumsum(axis=1)
    cum_table_new_user = table_new_user.cumsum(axis = 1)

    df_of_count_per_month = table_new_user.max().reset_index().set_index('subscription_start_month2')
    #first table - to check if accurate
    res = cum_table_new_user - cum_table_churn_user
    res = res.replace(0.0, np.nan).dropna(thresh = 1)
    res = remove_extra_months(res)
    return res, df_of_count_per_month


#-----------------------------------------------------------------------

def create_mrr_CohortAnalysisTable(pivot_mrr_churned_downgrades, pivot_mrr_paid, pivot_mrr_upgrades):  #plot12
    '''
    to create cohort analysis table by mrr count
    '''    
    full_list_action_col, full_list_cohort_grp_index = getFullListColAndIndex(pivot_mrr_churned_downgrades,pivot_mrr_paid) #action_month,cohort_month
    table_mrr_churned_downgrades = addMissingColAndIndexAndReSort(pivot_mrr_churned_downgrades, full_list_action_col, full_list_cohort_grp_index)
    table_mrr_paid = addMissingColAndIndexAndReSort(pivot_mrr_paid, full_list_action_col, full_list_cohort_grp_index)
    table_mrr_upgrades = addMissingColAndIndexAndReSort(pivot_mrr_upgrades, full_list_action_col, full_list_cohort_grp_index)

    cum_table_mrr_churn_downgrades = table_mrr_churned_downgrades.cumsum(axis=1)
    cum_table_mrr_paid = table_mrr_paid.cumsum(axis=1)
    cum_table_mrr_upgrades = table_mrr_upgrades.cumsum(axis=1)

    df_of_mrr_per_month = table_mrr_paid.max().reset_index().set_index('subscription_start_month2')
    #second table, need to check if accurate
    res = cum_table_mrr_paid + cum_table_mrr_upgrades - cum_table_mrr_churn_downgrades
    res = res.replace(0.0, np.nan).dropna(thresh = 1)
    res = remove_extra_months(res)
    return res, df_of_mrr_per_month
    
#-----------------------------------------------------------------------
    
def create_percentage_chart(res, df_of_count_per_month):
    num_col = len(list(res.columns))
    percent_res = res.copy()
    
    for idx in percent_res.index:
        percent_res.loc[idx] = [df_of_count_per_month.loc[idx][0]] * num_col
        
    res = (res/percent_res).round(decimals=5)
    res = res.replace(0.0, np.nan).dropna(thresh = 1)
    return res

#-----------------------------------------------------------------------

def reset_data_by_onboarding_month(res):
    num_col = len(res.columns)
    row = list(res.index)
    res_by_onboarding_month = pd.DataFrame(columns=range(0,num_col), index=row)

    counter = 0
    for idx in res_by_onboarding_month.index: 
        value_list = list(res.loc[idx].shift(-counter))
        res_by_onboarding_month.loc[idx] = value_list
        counter += 1

    res_by_onboarding_month = res_by_onboarding_month.replace(0,np.nan).dropna(thresh=1)

    return res_by_onboarding_month

#####################  Combine functions #####################
'''
These functions combine the above functions. Each function will return 2 tables - 1. main table chart 2. percent table chart
'''
def plot12(_pivot_user_churned, _pivot_user_paid): #user default cohort analysis
    
    basic_user_cohort_table, df_of_count_per_month = create_user_CohortAnalysisTable(_pivot_user_churned, _pivot_user_paid)
    basic_user_cohort_table_percent = create_percentage_chart(basic_user_cohort_table,df_of_count_per_month)

    return basic_user_cohort_table, basic_user_cohort_table_percent

def plot11(pivot_mrr_churned_downgrades, pivot_mrr_paid, pivot_mrr_upgrades): #mrr default cohort analysis
    basic_mrr_cohort_table, df_of_new_mrr_per_month = create_mrr_CohortAnalysisTable(pivot_mrr_churned_downgrades, pivot_mrr_paid, pivot_mrr_upgrades)
    basic_mrr_cohort_table_percent = create_percentage_chart(basic_mrr_cohort_table,df_of_new_mrr_per_month)

    return basic_mrr_cohort_table, basic_mrr_cohort_table_percent

def plot22(basic_user_cohort_table, basic_user_cohort_table_percent): #user onboarding month
    by_onboarding_month_user_cohort_table = reset_data_by_onboarding_month(basic_user_cohort_table)
    by_onboarding_month_user_cohort_table_percent = reset_data_by_onboarding_month(basic_user_cohort_table_percent)
    return by_onboarding_month_user_cohort_table, by_onboarding_month_user_cohort_table_percent

def plot21(basic_mrr_cohort_table, basic_mrr_cohort_table_percent): #mrr onboarding month
    by_onboarding_month_mrr_cohort_table = reset_data_by_onboarding_month(basic_mrr_cohort_table)
    by_onboarding_month_mrr_cohort_table_percent = reset_data_by_onboarding_month(basic_mrr_cohort_table_percent)
    return by_onboarding_month_mrr_cohort_table, by_onboarding_month_mrr_cohort_table_percent
    

#####################  Create plots #####################
#helper function - to remove extra months
def remove_extra_months(table):
    months_to_remove = []
    current_month = dt.datetime.now().strftime('%Y-%m')
    for col in table.columns.values:
        if col>current_month:
            months_to_remove.append(col)

    table = table.drop(months_to_remove, axis=1)
    
    return table

#-----------------------------------------------------------------------
def plotCharts(user_retention_table, percentFlag = False, mrrFlag = False, showFig = False, customFigName = '', customTitle = 'Cohorts: User Retention', numonboardingmonths = False):
    sns.set_context("poster", font_scale=55)
    format_values_as = '.1%' if percentFlag == True else '.0f'
    sns.set(style='white')
    plt.figure(figsize=(22, 10))
    plt.title(customTitle)
    ax = sns.heatmap(user_retention_table, mask=user_retention_table.isnull(), annot=True, fmt= format_values_as)
    if numonboardingmonths == True:
        plt.xlabel('Number of Onboarding Months')
    else:
        plt.xlabel('Cohort Month')
    plt.ylabel('Cohort Month')
    if mrrFlag == True and percentFlag == False:
        for t in ax.texts: t.set_text("$" + t.get_text())
    
    if customFigName != '':
        plt.savefig('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/static/img/cohortanalysis/' + customFigName)

    if showFig == True:
        plt.show()
    # improvements:
    # . to include total number of users somewhere
    # . improve on labels x and y axis

    
#-------------------- makes it cleaner to plot heatmap. makes use of plotCharts() ---------------------------------------------------
def plotOneChartEasy(tableNameAsStr, params_dict, showFig = False):
    dict_of_info = params_dict[tableNameAsStr]
    plotCharts(dict_of_info['table'], dict_of_info['percentFlag'], dict_of_info['mrrFlag'], \
               showFig, dict_of_info['customFigName'], dict_of_info['customTitle'], dict_of_info['numonboardingmonths'])

def plotAllChartsEasy(params_dict, showFig = False):
    for entry in params_dict:
        plotOneChartEasy(entry, params_dict, showFig = showFig)
#--------------------- plots retention curves --------------------------------------------------
def plotRetentionCurves(dataNameStr, create_plot_params_dict, showFig = False):
    data = create_plot_params_dict[dataNameStr]['table']
    title = create_plot_params_dict[dataNameStr]['customTitle']

    #add average to data
    data.loc['Average'] = data.mean()
    data = data.dropna(thresh=2, axis=1)
    plt.close('all')
    plt.figure(figsize=(10,8))
    sns.set(style='ticks')
    ax = plt.subplot()
    for idx in data.index.values:
        rowData = data.loc[idx].reset_index()
        if idx != 'Average' and idx > '2017-09':
            #sns.regplot('index', idx, data=rowData, order=2, label = idx)
            #sns.lmplot(idx, 'index', data=rowData)
            pass
        else:

            sns.regplot('index', idx, data=rowData, label = idx, ci = None, truncate=True, order=2)

    ax.legend()  
    plt.title(title )
    plt.savefig('/Users/brenda/Documents/Adhoc/reportingFlaskApp copy/static/img/cohortanalysis/retention_curves_' + dataNameStr)
    if showFig == True:
        plt.show()