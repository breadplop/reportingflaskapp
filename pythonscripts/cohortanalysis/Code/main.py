#can delete?
#from cleandata import *
import pythonscripts.cohortanalysis.Code.cleandata as c
#from cleandata import cleanRawData, raw_paid_users, raw_churned_downgrades, raw_upgrades, createPivotCharts, plot11, plot12, plot21,plot22, createMasterDict,plotAllChartsEasy, plotRetentionCurves

#import cleandata as c

cleanRawData, raw_paid_users, raw_churned_downgrades, raw_upgrades, createPivotCharts, plot11, plot12, plot21,plot22, createMasterDict,plotAllChartsEasy, plotRetentionCurves =c.cleanRawData, c.raw_paid_users, c.raw_churned_downgrades, c.raw_upgrades, c.createPivotCharts, c.plot11, c.plot12, c.plot21,c.plot22, c.createMasterDict,c.plotAllChartsEasy, c.plotRetentionCurves
if __name__ == '__main__':
#if __name__ == __name__: 

        #---------------------clean raw data--------------------------------------------------
        cleaned_paid = cleanRawData('Paid', raw_paid_users)
        print("went through")
        print (cleaned_paid.head())
        cleaned_churned_downgrades = cleanRawData('Lost', raw_churned_downgrades)
        cleaned_upgrades = cleanRawData('Upsell', raw_upgrades)

        #----------------------create pivot tables-------------------------------------------------
        pivot_mrr_paid = createPivotCharts('Paid','sum', cleaned_paid)
        pivot_user_paid = createPivotCharts('Paid','count', cleaned_paid).replace('',0.0)

        pivot_mrr_churned_downgrades = createPivotCharts('Lost','sum', cleaned_churned_downgrades)
        pivot_user_churned = createPivotCharts('Lost','count', cleaned_churned_downgrades).replace('',0.0)

        pivot_mrr_upgrades = createPivotCharts('Upsell','sum', cleaned_upgrades)

        #------------------------combine functions to get cohort analysis tables -----------------------------------------------
        basic_mrr_cohort_table11, basic_mrr_cohort_table_percent11 =  plot11(pivot_mrr_churned_downgrades, pivot_mrr_paid, pivot_mrr_upgrades)
        basic_user_cohort_table12, basic_user_cohort_table_percent12 = plot12(pivot_user_churned, pivot_user_paid)

        by_onboarding_month_mrr_cohort_table21, by_onboarding_month_mrr_cohort_table_percent21 = plot21(basic_mrr_cohort_table11, basic_mrr_cohort_table_percent11)
        by_onboarding_month_user_cohort_table22, by_onboarding_month_user_cohort_table_percent22 = plot22(basic_user_cohort_table12, basic_user_cohort_table_percent12)

        list_of_tables = [basic_mrr_cohort_table11, basic_mrr_cohort_table_percent11, \
                 basic_user_cohort_table12, basic_user_cohort_table_percent12, \
                 by_onboarding_month_mrr_cohort_table21, by_onboarding_month_mrr_cohort_table_percent21,\
                 by_onboarding_month_user_cohort_table22, by_onboarding_month_user_cohort_table_percent22]
        
        create_plot_params_dict = createMasterDict(list_of_tables)
        #---------------------- create plots -------------------------------------------------
        plotAllChartsEasy(create_plot_params_dict, showFig=False)

        #----------------------plot retention cuves -------------------------------------------------
        plotRetentionCurves('by_onboarding_month_mrr_cohort_table_percent21',create_plot_params_dict)
        plotRetentionCurves('by_onboarding_month_user_cohort_table_percent22',create_plot_params_dict)

