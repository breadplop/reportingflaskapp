# reportingflaskapp


### Checklist Table
| Feature | Priority | Est Time | Invested Time | Actual Time |
| ------- | -------- | -------- | ------------- | ----------- |
|f |f |f f| f| f|
| ------- | -------- | -------- | ------------- | ----------- |
| ------- | -------- | -------- | ------------- | ----------- |

---


# How to update/change


## Feature Audit
### To update:
`PlotDefault()` function in feature_audit_tabulate2.py file 
- to change start and end month


### Security:
- [ ] to remove API keys


## uh, need to create database
http://blog.y3xz.com/blog/2012/08/16/flask-and-postgresql-on-heroku



### what i did:
![ref](http://www.patricksoftwareblog.com/database-using-postgresql-and-sqlalchemy/)
CREATE DATABASE stickiness_ratio_data OWNER brenda;


### psql commands
- \l
- \q

### how to use git
```
(ffr_env) $ git status
(ffr_env) $ git add .
(ffr_env) $ git commit -m "Added SQLAlchemy support and file for initializing database"
(ffr_env) $ git checkout master
(ffr_env) $ git merge add_database
(ffr_env) $ git branch -d add_database
(ffr_env) $ git push -u origin master
```


# cohort analysis by segment